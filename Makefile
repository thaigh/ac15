PROJ=AC15
TEST=AC15.Tests
EXE=AC15/AC15.csproj

clean:
	dotnet clean $(PROJ)
	dotnet clean $(TEST)
	rm -rf $(COVERAGE_REPORT_ROOT)

build:
	dotnet restore --configfile NuGet.config
	dotnet build $(PROJ) -c $(BUILD_TARGET)
	dotnet build $(TEST) -c $(BUILD_TARGET)

run: clean build
	dotnet run --project $(EXE)

test: clean build
	dotnet test $(TEST) -c $(BUILD_TARGET)

coverage: clean build test
	sh coverage.sh

all: clean build



# Coverage Test Support

OPEN_COVER=D:/App/OpenCover/OpenCover.Console.exe
DOTNET="C:\Program Files\dotnet\dotnet.exe"
REPORT_GENERATOR=D:/App/ReportGenerator/ReportGenerator.exe
BUILD_FRAMEWORK=netcoreapp2.0
BUILD_TARGET=Debug

COVERAGE_REPORT_ROOT=CoverageReports
COVERAGE_REPORT_INPUT=$(COVERAGE_REPORT_ROOT)/AC15.Tests.coverage.xml
COVERAGE_REPORT_OUTPUT=$(COVERAGE_REPORT_ROOT)/AC15.Tests.coverage.report

test-coverage: clean build
	mkdir -p $(COVERAGE_REPORT_ROOT)
	$(OPEN_COVER) \
	-register:user \
	-target:$(DOTNET) \
	-targetargs:"test -f $(BUILD_FRAMEWORK) -c $(BUILD_TARGET) $(TEST)" \
	-output:$(COVERAGE_REPORT_INPUT) \
	-oldStyle

test-coverage-report:
	rm -rf $(COVERAGE_REPORT_OUTPUT)
	$(REPORT_GENERATOR) \
	-reports:$(COVERAGE_REPORT_INPUT) \
	-targetdir:$(COVERAGE_REPORT_OUTPUT) \

test-coverage-all: test-coverage test-coverage-report	