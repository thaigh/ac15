﻿using AC15.Instructions;
using AC15.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Instructions
{
    public partial class InstructionTests
    {
        [Fact]
        public void NegateInstruction_Constructor()
        {
            NegateInstruction inst = new NegateInstruction();
            Assert.Equal(OpCode.Negate, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new BooleanMemory(false));
            Assert.Equal("[NegateInstruction: Code=Negate]", inst.ToString());
        }

        [Fact]
        public void BooleanEqualToInstruction_Constructor_Uninitialised()
        {
            BooleanEqualToInstruction inst = new BooleanEqualToInstruction();
            Assert.Equal(OpCode.BooleanEqualTo, inst.Code);
            Assert.IsType<BooleanMemory>(inst.EffectiveOperand);
            Assert.Equal(false, ((BooleanMemory)inst.EffectiveOperand).Value);
            Assert.Equal(false, inst.Value);
            Assert.Equal("[BooleanEqualToInstruction: Code=BooleanEqualTo, EffectiveOperand=[BooleanMemory: Value=False]]", inst.ToString());
        }

        [Fact]
        public void BooleanEqualToInstruction_Constructor_Initialised()
        {
            BooleanEqualToInstruction inst = new BooleanEqualToInstruction(true);
            Assert.Equal(OpCode.BooleanEqualTo, inst.Code);
            Assert.IsType<BooleanMemory>(inst.EffectiveOperand);
            Assert.Equal(true, ((BooleanMemory)inst.EffectiveOperand).Value);
            Assert.Equal(true, inst.Value);
            Assert.Equal("[BooleanEqualToInstruction: Code=BooleanEqualTo, EffectiveOperand=[BooleanMemory: Value=True]]", inst.ToString());
        }

        [Fact]
        public void BooleanNotEqualToInstruction_Constructor_Uninitialised()
        {
            BooleanNotEqualToInstruction inst = new BooleanNotEqualToInstruction();
            Assert.Equal(OpCode.BooleanNotEqualTo, inst.Code);
            Assert.IsType<BooleanMemory>(inst.EffectiveOperand);
            Assert.Equal(false, ((BooleanMemory)inst.EffectiveOperand).Value);
            Assert.Equal(false, inst.Value);
            Assert.Equal("[BooleanNotEqualToInstruction: Code=BooleanNotEqualTo, EffectiveOperand=[BooleanMemory: Value=False]]", inst.ToString());
        }

        [Fact]
        public void BooleanNotEqualToInstruction_Constructor_Initialised()
        {
            BooleanNotEqualToInstruction inst = new BooleanNotEqualToInstruction(true);
            Assert.Equal(OpCode.BooleanNotEqualTo, inst.Code);
            Assert.IsType<BooleanMemory>(inst.EffectiveOperand);
            Assert.Equal(true, ((BooleanMemory)inst.EffectiveOperand).Value);
            Assert.Equal(true, inst.Value);
            Assert.Equal("[BooleanNotEqualToInstruction: Code=BooleanNotEqualTo, EffectiveOperand=[BooleanMemory: Value=True]]", inst.ToString());
        }

        [Fact]
        public void BooleanAndInstruction_Constructor_Uninitialised()
        {
            BooleanAndInstruction inst = new BooleanAndInstruction();
            Assert.Equal(OpCode.And, inst.Code);
            Assert.IsType<BooleanMemory>(inst.EffectiveOperand);
            Assert.Equal(false, ((BooleanMemory)inst.EffectiveOperand).Value);
            Assert.Equal(false, inst.Value);
            Assert.Equal("[BooleanAndInstruction: Code=And, EffectiveOperand=[BooleanMemory: Value=False]]", inst.ToString());
        }

        [Fact]
        public void BooleanAndInstruction_Constructor_Initialised()
        {
            BooleanAndInstruction inst = new BooleanAndInstruction(true);
            Assert.Equal(OpCode.And, inst.Code);
            Assert.IsType<BooleanMemory>(inst.EffectiveOperand);
            Assert.Equal(true, ((BooleanMemory)inst.EffectiveOperand).Value);
            Assert.Equal(true, inst.Value);
            Assert.Equal("[BooleanAndInstruction: Code=And, EffectiveOperand=[BooleanMemory: Value=True]]", inst.ToString());
        }

        [Fact]
        public void BooleanOrInstruction_Constructor_Uninitialised()
        {
            BooleanOrInstruction inst = new BooleanOrInstruction();
            Assert.Equal(OpCode.Or, inst.Code);
            Assert.IsType<BooleanMemory>(inst.EffectiveOperand);
            Assert.Equal(false, ((BooleanMemory)inst.EffectiveOperand).Value);
            Assert.Equal(false, inst.Value);
            Assert.Equal("[BooleanOrInstruction: Code=Or, EffectiveOperand=[BooleanMemory: Value=False]]", inst.ToString());
        }

        [Fact]
        public void BooleanOrInstruction_Constructor_Initialised()
        {
            BooleanOrInstruction inst = new BooleanOrInstruction(true);
            Assert.Equal(OpCode.Or, inst.Code);
            Assert.IsType<BooleanMemory>(inst.EffectiveOperand);
            Assert.Equal(true, ((BooleanMemory)inst.EffectiveOperand).Value);
            Assert.Equal(true, inst.Value);
            Assert.Equal("[BooleanOrInstruction: Code=Or, EffectiveOperand=[BooleanMemory: Value=True]]", inst.ToString());
        }

        [Fact]
        public void BooleanXorInstruction_Constructor_Uninitialised()
        {
            BooleanXorInstruction inst = new BooleanXorInstruction();
            Assert.Equal(OpCode.Xor, inst.Code);
            Assert.IsType<BooleanMemory>(inst.EffectiveOperand);
            Assert.Equal(false, ((BooleanMemory)inst.EffectiveOperand).Value);
            Assert.Equal(false, inst.Value);
            Assert.Equal("[BooleanXorInstruction: Code=Xor, EffectiveOperand=[BooleanMemory: Value=False]]", inst.ToString());
        }

        [Fact]
        public void BooleanXorInstruction_Constructor_Initialised()
        {
            BooleanXorInstruction inst = new BooleanXorInstruction(true);
            Assert.Equal(OpCode.Xor, inst.Code);
            Assert.IsType<BooleanMemory>(inst.EffectiveOperand);
            Assert.Equal(true, ((BooleanMemory)inst.EffectiveOperand).Value);
            Assert.Equal(true, inst.Value);
            Assert.Equal("[BooleanXorInstruction: Code=Xor, EffectiveOperand=[BooleanMemory: Value=True]]", inst.ToString());
        }
    }
}
