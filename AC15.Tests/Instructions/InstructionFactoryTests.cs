﻿using AC15.Instructions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Instructions
{
    public class InstructionFactoryTests
    {

        [Fact]
        public void InstructionFactory_CreateAddOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.Add);
            Assert.IsType<AddInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateSubtractOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.Subtract);
            Assert.IsType<SubtractInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateMultiplyOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.Multiply);
            Assert.IsType<MultiplyInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateDivideOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.Divide);
            Assert.IsType<DivideInstruction>(inst);
        }



        [Fact]
        public void InstructionFactory_CreateGetOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.Get);
            Assert.IsType<GetInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateStoreOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.Store);
            Assert.IsType<StoreInstruction>(inst);
        }



        [Fact]
        public void InstructionFactory_CreateHaltOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.Halt);
            Assert.IsType<HaltInstruction>(inst);
        }



        [Fact]
        public void InstructionFactory_CreateNegateOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.Negate);
            Assert.IsType<NegateInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateBooleanEqualToOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.BooleanEqualTo);
            Assert.IsType<BooleanEqualToInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateBooleanNotEqualToOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.BooleanNotEqualTo);
            Assert.IsType<BooleanNotEqualToInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateAndOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.And);
            Assert.IsType<BooleanAndInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateOrOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.Or);
            Assert.IsType<BooleanOrInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateXorOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.Xor);
            Assert.IsType<BooleanXorInstruction>(inst);
        }



        [Fact]
        public void InstructionFactory_CreateReadFloatOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.ReadFloat);
            Assert.IsType<ReadFloatInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateReadIntOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.ReadInt);
            Assert.IsType<ReadIntegerInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateValuePrintOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.ValuePrint);
            Assert.IsType<ValuePrintInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateStringPrintOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.StringPrint);
            Assert.IsType<StringPrintInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateNewLineOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.NewLine);
            Assert.IsType<NewLineInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateSpaceOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.Space);
            Assert.IsType<SpacePrintInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateGreaterThanOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.GreaterThan);
            Assert.IsType<GreaterThanInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateGreaterThanEqualOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.GreaterThanEqual);
            Assert.IsType<GreaterThanOrEqualInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateLessThanOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.LessThan);
            Assert.IsType<LessThanInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateLessThanEqualOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.LessThanEqual);
            Assert.IsType<LessThanOrEqualInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateEqualToOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.EqualTo);
            Assert.IsType<EqualToInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateNotEqualToOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.NotEqualTo);
            Assert.IsType<NotEqualToInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateAccumulatorToIndexOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.AccumulatorToIndex);
            Assert.IsType<AccumulatorToIndexInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateIncrementIndexOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.IncrementIndex);
            Assert.IsType<IncrementIndexInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateDecrementIndexOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.DecrementIndex);
            Assert.IsType<DecrementIndexInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateResetIndexOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.ResetIndex);
            Assert.IsType<ResetIndexInstruction>(inst);
        }

        [Fact]
        public void InstructionFactory_CreateLoadIndexOpCode()
        {
            InstructionFactory factory = new InstructionFactory();
            IInstruction inst = factory.Create(OpCode.LoadIndex);
            Assert.IsType<LoadIndexInstruction>(inst);
        }



        [Fact]
        public void InstructionFactory_CreateUnmappedOpCode_ThrowArgumentException()
        {
            InstructionFactory factory = new InstructionFactory();
            Assert.Throws<ArgumentException>(() => factory.Create((OpCode)999));
        }

    }
}
