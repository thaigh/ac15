﻿using AC15.Instructions;
using AC15.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Instructions
{
    public partial class InstructionTests
    {
        [Fact]
        public void ReadFloatInstruction_Constructor()
        {
            ReadFloatInstruction inst = new ReadFloatInstruction();
            Assert.Equal(OpCode.ReadFloat, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(1));
            Assert.Equal("[ReadFloatInstruction Code=ReadFloat]", inst.ToString());
        }

        [Fact]
        public void ReadIntegerInstruction_Constructor()
        {
            ReadIntegerInstruction inst = new ReadIntegerInstruction();
            Assert.Equal(OpCode.ReadInt, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(1));
            Assert.Equal("[ReadIntegerInstruction Code=ReadInt]", inst.ToString());
        }

        [Fact]
        public void NewLineInstruction_Constructor_Uninitialised()
        {
            NewLineInstruction inst = new NewLineInstruction();
            Assert.Equal(OpCode.NewLine, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(1));
            Assert.Equal("[NewLineInstruction]", inst.ToString());
        }

        [Fact]
        public void ValuePrintInstruction_Constructor_Uninitialised()
        {
            ValuePrintInstruction inst = new ValuePrintInstruction();
            Assert.Equal(OpCode.ValuePrint, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(1));
            Assert.Equal("[ValuePrintInstruction]", inst.ToString());
        }

        [Fact]
        public void SpacePrintInstruction_Constructor_Uninitialised()
        {
            SpacePrintInstruction inst = new SpacePrintInstruction();
            Assert.Equal(OpCode.Space, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(1));
            Assert.Equal("[SpacePrintInstruction]", inst.ToString());
        }

        [Fact]
        public void StringPrintInstruction_Constructor_Uninitialised()
        {
            StringPrintInstruction inst = new StringPrintInstruction();
            Assert.Equal(OpCode.StringPrint, inst.Code);

            Assert.IsType<StringMemory>(inst.EffectiveOperand);
            StringMemory mem = (StringMemory)inst.EffectiveOperand;
            Assert.Equal("", mem.CharacterString);

            Assert.Equal("[StringPrintInstruction Value=]", inst.ToString());
        }

        [Fact]
        public void StringPrintInstruction_Constructor_Initialised()
        {
            StringPrintInstruction inst = new StringPrintInstruction("Hello");
            Assert.Equal(OpCode.StringPrint, inst.Code);

            Assert.IsType<StringMemory>(inst.EffectiveOperand);
            StringMemory mem = (StringMemory)inst.EffectiveOperand;
            Assert.Equal("Hello", mem.CharacterString);

            Assert.Equal("[StringPrintInstruction Value=Hello]", inst.ToString());
        }

    }
}
