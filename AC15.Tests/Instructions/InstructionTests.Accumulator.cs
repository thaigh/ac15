﻿using AC15.Instructions;
using AC15.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Instructions
{
    public partial class InstructionTests
    {

        [Fact]
        public void HaltInstruction_Constructor_Uninitialised()
        {
            HaltInstruction inst = new HaltInstruction();
            Assert.Equal(OpCode.Halt, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(1));
            Assert.Equal("[HaltInstruction: Code=Halt]", inst.ToString());
        }

    }
}
