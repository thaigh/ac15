﻿using AC15.Instructions;
using AC15.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Instructions
{
    public partial class InstructionTests
    {

        [Fact]
        public void AddInstruction_Constructor_Uninitialised()
        {
            AddInstruction inst = new AddInstruction();
            Assert.Equal(OpCode.Add, inst.Code);
            Assert.IsType<FloatingPointMemory>(inst.EffectiveOperand);
            Assert.Equal(0, ((FloatingPointMemory)inst.EffectiveOperand).Value);
            Assert.Equal(0, inst.Value);
            Assert.Equal("[AddInstruction: Code=Add, EffectiveOperand=[FloatingPointMemory: Value=0]]", inst.ToString());
        }

        [Fact]
        public void AddInstruction_Constructor_Initialised()
        {
            AddInstruction inst = new AddInstruction(14);
            Assert.Equal(OpCode.Add, inst.Code);
            Assert.IsType<FloatingPointMemory>(inst.EffectiveOperand);
            Assert.Equal(14, ((FloatingPointMemory)inst.EffectiveOperand).Value);
            Assert.Equal(14, inst.Value);
            Assert.Equal("[AddInstruction: Code=Add, EffectiveOperand=[FloatingPointMemory: Value=14]]", inst.ToString());
        }



        [Fact]
        public void SubtractInstruction_Constructor_Uninitialised()
        {
            SubtractInstruction inst = new SubtractInstruction();
            Assert.Equal(OpCode.Subtract, inst.Code);
            Assert.IsType<FloatingPointMemory>(inst.EffectiveOperand);
            Assert.Equal(0, ((FloatingPointMemory)inst.EffectiveOperand).Value);
            Assert.Equal(0, inst.Value);
            Assert.Equal("[SubtractInstruction: Code=Subtract, EffectiveOperand=[FloatingPointMemory: Value=0]]", inst.ToString());
        }

        [Fact]
        public void SubtractInstruction_Constructor_Initialised()
        {
            SubtractInstruction inst = new SubtractInstruction(14);
            Assert.Equal(OpCode.Subtract, inst.Code);
            Assert.IsType<FloatingPointMemory>(inst.EffectiveOperand);
            Assert.Equal(14, ((FloatingPointMemory)inst.EffectiveOperand).Value);
            Assert.Equal(14, inst.Value);
            Assert.Equal("[SubtractInstruction: Code=Subtract, EffectiveOperand=[FloatingPointMemory: Value=14]]", inst.ToString());
        }



        [Fact]
        public void MultiplyInstruction_Constructor_Uninitialised()
        {
            MultiplyInstruction inst = new MultiplyInstruction();
            Assert.Equal(OpCode.Multiply, inst.Code);
            Assert.IsType<FloatingPointMemory>(inst.EffectiveOperand);
            Assert.Equal(0, ((FloatingPointMemory)inst.EffectiveOperand).Value);
            Assert.Equal(0, inst.Value);
            Assert.Equal("[MultiplyInstruction: Code=Multiply, EffectiveOperand=[FloatingPointMemory: Value=0]]", inst.ToString());
        }

        [Fact]
        public void MultiplyInstruction_Constructor_Initialised()
        {
            MultiplyInstruction inst = new MultiplyInstruction(14);
            Assert.Equal(OpCode.Multiply, inst.Code);
            Assert.IsType<FloatingPointMemory>(inst.EffectiveOperand);
            Assert.Equal(14, ((FloatingPointMemory)inst.EffectiveOperand).Value);
            Assert.Equal(14, inst.Value);
            Assert.Equal("[MultiplyInstruction: Code=Multiply, EffectiveOperand=[FloatingPointMemory: Value=14]]", inst.ToString());
        }



        [Fact]
        public void DivideInstruction_Constructor_Uninitialised()
        {
            DivideInstruction inst = new DivideInstruction();
            Assert.Equal(OpCode.Divide, inst.Code);
            Assert.IsType<FloatingPointMemory>(inst.EffectiveOperand);
            Assert.Equal(0, ((FloatingPointMemory)inst.EffectiveOperand).Value);
            Assert.Equal(0, inst.Value);
            Assert.Equal("[DivideInstruction: Code=Divide, EffectiveOperand=[FloatingPointMemory: Value=0]]", inst.ToString());
        }

        [Fact]
        public void DivideInstruction_Constructor_Initialised()
        {
            DivideInstruction inst = new DivideInstruction(14);
            Assert.Equal(OpCode.Divide, inst.Code);
            Assert.IsType<FloatingPointMemory>(inst.EffectiveOperand);
            Assert.Equal(14, ((FloatingPointMemory)inst.EffectiveOperand).Value);
            Assert.Equal(14, inst.Value);
            Assert.Equal("[DivideInstruction: Code=Divide, EffectiveOperand=[FloatingPointMemory: Value=14]]", inst.ToString());
        }



        [Fact]
        public void GreaterThanInstruction_Constructor()
        {
            GreaterThanInstruction inst = new GreaterThanInstruction();
            Assert.Equal(OpCode.GreaterThan, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(1));
            Assert.Equal("[GreaterThanInstruction: Code=GreaterThan]", inst.ToString());
        }

        [Fact]
        public void GreaterThanOrEqualInstruction_Constructor()
        {
            GreaterThanOrEqualInstruction inst = new GreaterThanOrEqualInstruction();
            Assert.Equal(OpCode.GreaterThanEqual, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(1));
            Assert.Equal("[GreaterThanOrEqualInstruction: Code=GreaterThanEqual]", inst.ToString());
        }

        [Fact]
        public void LessThanInstruction_Constructor()
        {
            LessThanInstruction inst = new LessThanInstruction();
            Assert.Equal(OpCode.LessThan, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(1));
            Assert.Equal("[LessThanInstruction: Code=LessThan]", inst.ToString());
        }

        [Fact]
        public void LessThanOrEqualInstruction_Constructor()
        {
            LessThanOrEqualInstruction inst = new LessThanOrEqualInstruction();
            Assert.Equal(OpCode.LessThanEqual, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(1));
            Assert.Equal("[LessThanOrEqualInstruction: Code=LessThanEqual]", inst.ToString());
        }

        [Fact]
        public void EqualToInstruction_Constructor()
        {
            EqualToInstruction inst = new EqualToInstruction();
            Assert.Equal(OpCode.EqualTo, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(1));
            Assert.Equal("[EqualToInstruction: Code=EqualTo]", inst.ToString());
        }

        [Fact]
        public void NotEqualToInstruction_Constructor()
        {
            NotEqualToInstruction inst = new NotEqualToInstruction();
            Assert.Equal(OpCode.NotEqualTo, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(1));
            Assert.Equal("[NotEqualToInstruction: Code=NotEqualTo]", inst.ToString());
        }

    }
}
