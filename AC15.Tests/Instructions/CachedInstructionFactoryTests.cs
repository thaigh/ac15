﻿using AC15.Instructions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Instructions
{
    public class CachedInstructionFactoryTests
    {

        [Fact]
        public void CachedInstructionFactory_Create_CreateCachedKey()
        {
            CachedInstructionFactory factory = new CachedInstructionFactory();
            IInstruction inst = factory.Create(OpCode.Add);

            Assert.IsType<AddInstruction>(inst);
        }

        [Fact]
        public void CachedInstructionFactory_Create_UsedCachedKey()
        {
            CachedInstructionFactory factory = new CachedInstructionFactory();

            IInstruction instOne = factory.Create(OpCode.Add);
            Assert.IsType<AddInstruction>(instOne);
            ((AddInstruction)instOne).Value = 13;


            IInstruction instTwo = factory.Create(OpCode.Add);
            Assert.IsType<AddInstruction>(instTwo);

            Assert.Same(instOne, instTwo);
            Assert.Same(instOne.EffectiveOperand, instTwo.EffectiveOperand);
        }

    }
}
