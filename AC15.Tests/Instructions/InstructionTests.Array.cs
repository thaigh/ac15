using AC15.Instructions;
using AC15.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Instructions
{
    public partial class InstructionTests
    {
        [Fact]
        public void AccumulatorToIndexInstruction_Constructor()
        {
            AccumulatorToIndexInstruction inst = new AccumulatorToIndexInstruction();
            Assert.Equal(OpCode.AccumulatorToIndex, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(0));
            Assert.Equal("[AccumulatorToIndexInstruction: Code=AccumulatorToIndex]", inst.ToString());
        }

        [Fact]
        public void IncrementIndexInstruction_Constructor()
        {
            IncrementIndexInstruction inst = new IncrementIndexInstruction();
            Assert.Equal(OpCode.IncrementIndex, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(0));
            Assert.Equal("[IncrementIndexInstruction: Code=IncrementIndex]", inst.ToString());
        }

        [Fact]
        public void DecrementIndexInstruction_Constructor()
        {
            DecrementIndexInstruction inst = new DecrementIndexInstruction();
            Assert.Equal(OpCode.DecrementIndex, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(0));
            Assert.Equal("[DecrementIndexInstruction: Code=DecrementIndex]", inst.ToString());
        }

        [Fact]
        public void ResetIndexInstruction_Constructor()
        {
            ResetIndexInstruction inst = new ResetIndexInstruction();
            Assert.Equal(OpCode.ResetIndex, inst.Code);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand);
            Assert.Throws<InvalidOperationException>(() => inst.EffectiveOperand = new FloatingPointMemory(0));
            Assert.Equal("[ResetIndexInstruction: Code=ResetIndex]", inst.ToString());
        }

        [Fact]
        public void LoadIndexInstruction_Constructor_Uninitialised()
        {
            LoadIndexInstruction inst = new LoadIndexInstruction();
            Assert.Equal(OpCode.LoadIndex, inst.Code);
            Assert.IsType<MemoryAddress>(inst.EffectiveOperand);
            Assert.Equal(0, ((MemoryAddress)inst.EffectiveOperand).AddressIndex);
            Assert.Equal(0, inst.AddressIndex);
            Assert.Equal("[LoadIndexInstruction: Code=LoadIndex, AddressIndex=0]", inst.ToString());
        }

        [Fact]
        public void LoadIndexInstruction_Constructor_Initialised()
        {
            LoadIndexInstruction inst = new LoadIndexInstruction(12);
            Assert.Equal(OpCode.LoadIndex, inst.Code);
            Assert.IsType<MemoryAddress>(inst.EffectiveOperand);
            Assert.Equal(12, ((MemoryAddress)inst.EffectiveOperand).AddressIndex);
            Assert.Equal(12, inst.AddressIndex);
            Assert.Equal("[LoadIndexInstruction: Code=LoadIndex, AddressIndex=12]", inst.ToString());
        }
    }
}