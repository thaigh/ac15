﻿using AC15.Instructions;
using AC15.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Instructions
{
    public partial class InstructionTests
    {

        [Fact]
        public void GetInstruction_Constructor_Uninitialised()
        {
            GetInstruction inst = new GetInstruction();
            Assert.Equal(OpCode.Get, inst.Code);
            Assert.IsType<MemoryAddress>(inst.EffectiveOperand);
            Assert.Equal(0, ((MemoryAddress)inst.EffectiveOperand).AddressIndex);
            Assert.Equal(0, inst.Value);
            Assert.Equal("[GetInstruction: Code=Get, EffectiveOperand=[MemoryAddress: AddressIndex=0], Value=0]", inst.ToString());
        }

        [Fact]
        public void GetInstruction_Constructor_Initialised()
        {
            GetInstruction inst = new GetInstruction(14);
            Assert.Equal(OpCode.Get, inst.Code);
            Assert.IsType<MemoryAddress>(inst.EffectiveOperand);
            Assert.Equal(14, ((MemoryAddress)inst.EffectiveOperand).AddressIndex);
            Assert.Equal(14, inst.Value);
            Assert.Equal("[GetInstruction: Code=Get, EffectiveOperand=[MemoryAddress: AddressIndex=14], Value=14]", inst.ToString());
        }



        [Fact]
        public void StoreInstruction_Constructor_Uninitialised()
        {
            StoreInstruction inst = new StoreInstruction();
            Assert.Equal(OpCode.Store, inst.Code);
            Assert.IsType<MemoryAddress>(inst.EffectiveOperand);
            Assert.Equal(0, ((MemoryAddress)inst.EffectiveOperand).AddressIndex);
            Assert.Equal(0, inst.Value);
            Assert.Equal("[StoreInstruction: Code=Store, EffectiveOperand=[MemoryAddress: AddressIndex=0], Value=0]", inst.ToString());
        }

        [Fact]
        public void StoreInstruction_Constructor_Initialised()
        {
            StoreInstruction inst = new StoreInstruction(14);
            Assert.Equal(OpCode.Store, inst.Code);
            Assert.IsType<MemoryAddress>(inst.EffectiveOperand);
            Assert.Equal(14, ((MemoryAddress)inst.EffectiveOperand).AddressIndex);
            Assert.Equal(14, inst.Value);
            Assert.Equal("[StoreInstruction: Code=Store, EffectiveOperand=[MemoryAddress: AddressIndex=14], Value=14]", inst.ToString());
        }

    }
}
