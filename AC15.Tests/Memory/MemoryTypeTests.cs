﻿using AC15.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Memory
{
    public partial class MemoryTypeTests
    {

        [Fact]
        public void ArrayDescriptor_Constructor()
        {
            ArrayDescriptor ab = new ArrayDescriptor(3, 4);

            Assert.Equal(MemoryType.ArrayDescriptor, ab.MemoryType);
            Assert.Equal(3, ab.ArraySize);
            Assert.Equal(4, ab.ArrayStartAddress);
            Assert.Equal("[ArrayDescriptor: ArrayStartAddress=4, ArraySize=3]", ab.ToString());
        }

        [Fact]
        public void FloatingPointMemory_Constructor()
        {
            FloatingPointMemory ab = new FloatingPointMemory(4);

            Assert.Equal(MemoryType.FloatingPoint, ab.MemoryType);
            Assert.Equal(4, ab.Value);
            Assert.Equal("[FloatingPointMemory: Value=4]", ab.ToString());
        }

        [Fact]
        public void MemoryAddress_Constructor()
        {
            MemoryAddress ab = new MemoryAddress(4);

            Assert.Equal(MemoryType.MemoryAddress, ab.MemoryType);
            Assert.Equal(4, ab.AddressIndex);
            Assert.Equal("[MemoryAddress: AddressIndex=4]", ab.ToString());
        }

        [Fact]
        public void SubprogramDescriptor_Constructor()
        {
            SubprogramDescriptor ab = new SubprogramDescriptor(4, 5);

            Assert.Equal(MemoryType.SubprogramDescriptor, ab.MemoryType);
            Assert.Equal(4, ab.EntryAddress);
            Assert.Equal(5, ab.ReturnAddress);
            Assert.Equal("[SubprogramDescriptor: EntryAddress=4, ReturnAddress=5]", ab.ToString());
        }
    }
}
