﻿using AC15.Exceptions;
using AC15.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Memory
{
    public partial class MemoryTypeTests
    {

        [Fact]
        public void StringMemory_EmptyConstructor()
        {
            StringMemory mem = new StringMemory();
            Assert.Equal(MemoryType.String, mem.MemoryType);
            Assert.Equal("", mem.CharacterString);
            Assert.Equal(StringMemory.NullString.Characters, mem.Characters);
            Assert.Equal("[String: [\0, \0, \0, \0, \0, \0, \0, \0]]", mem.ToString());
        }

        [Fact]
        public void StringMemory_InitialisedConstructor_EmptyCharArray()
        {
            StringMemory mem = new StringMemory(new char[] { });
            Assert.Equal(MemoryType.String, mem.MemoryType);
            Assert.Equal("", mem.CharacterString);
            Assert.Equal(StringMemory.NullString.Characters, mem.Characters);
            Assert.Equal("[String: [\0, \0, \0, \0, \0, \0, \0, \0]]", mem.ToString());
        }

        [Fact]
        public void StringMemory_InitialisedConstructor_OneCharArray()
        {
            StringMemory mem = new StringMemory(new char[] { 'h' });
            Assert.Equal(MemoryType.String, mem.MemoryType);
            Assert.Equal("h", mem.CharacterString);
            Assert.Equal(new char[] { 'h', '\0', '\0', '\0',   '\0', '\0', '\0', '\0' }, mem.Characters);
            Assert.Equal("[String: [h, \0, \0, \0, \0, \0, \0, \0]]", mem.ToString());
        }

        [Fact]
        public void StringMemory_InitialisedConstructor_EightCharArray()
        {
            StringMemory mem = new StringMemory(new char[] { 'h', 'e', 'l', 'l',   'o', ' ', 'w', 'o' });
            Assert.Equal(MemoryType.String, mem.MemoryType);
            Assert.Equal("hello wo", mem.CharacterString);
            Assert.Equal(new char[] { 'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o' }, mem.Characters);
            Assert.Equal("[String: [h, e, l, l, o,  , w, o]]", mem.ToString());
        }

        [Fact]
        public void StringMemory_InitialisedConstructor_NineCharArray_ThrowsInvalidStringException()
        {
            StringMemory mem;
            Assert.Throws<InvalidStringException>( () => mem = new StringMemory(new char[] { 'h', 'e', 'l', 'l',    'o', ' ', 'w', 'o',    'r'}));
        }

        [Fact]
        public void StringMemory_InitialisedConstructor_TwelveCharacterString_ThrowsInvalidStringException()
        {
            StringMemory mem;
            Assert.Throws<InvalidStringException>(() => mem = new StringMemory("hello world!"));
        }

        [Fact]
        public void StringMemory_InitialisedConstructor_EightCharacterString()
        {
            StringMemory mem = new StringMemory("hello wo");
            Assert.Equal(MemoryType.String, mem.MemoryType);
            Assert.Equal("hello wo", mem.CharacterString);
            Assert.Equal(new char[] { 'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o' }, mem.Characters);
            Assert.Equal("[String: [h, e, l, l, o,  , w, o]]", mem.ToString());
        }
    }
}
