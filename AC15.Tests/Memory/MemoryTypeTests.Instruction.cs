﻿using AC15.Exceptions;
using AC15.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Memory
{
    public partial class MemoryTypeTests
    {

        [Fact]
        public void InstructionMemory_DefaultConstructor()
        {
            InstructionMemory mem = new InstructionMemory();
            Assert.Equal(MemoryType.Instruction, mem.MemoryType);
            Assert.Equal(InstructionMemory.NoOperation.Bytes, mem.Bytes);
            Assert.Equal("[Instruction: Bytes=[1, 1, 1, 1, 1, 1, 1, 1]]", mem.ToString());
        }


        [Fact]
        public void InstructionMemory_InitialisedConstructor_EmptyArray()
        {
            InstructionMemory mem = new InstructionMemory(new int[] { });
            Assert.Equal(MemoryType.Instruction, mem.MemoryType);
            Assert.Equal(InstructionMemory.NoOperation.Bytes, mem.Bytes);
            Assert.Equal("[Instruction: Bytes=[1, 1, 1, 1, 1, 1, 1, 1]]", mem.ToString());
        }

        [Fact]
        public void InstructionMemory_InitialisedConstructor_OneElement()
        {
            InstructionMemory mem = new InstructionMemory(new int[] { 23 });
            Assert.Equal(MemoryType.Instruction, mem.MemoryType);
            Assert.Equal(new int[] { 23, 1, 1, 1, 1, 1, 1, 1 }, mem.Bytes);
            Assert.Equal("[Instruction: Bytes=[23, 1, 1, 1, 1, 1, 1, 1]]", mem.ToString());
        }

        [Fact]
        public void InstructionMemory_InitialisedConstructor_EightElements()
        {
            InstructionMemory mem = new InstructionMemory(new int[] { 2,3,4,5, 6,7,8,9 });
            Assert.Equal(MemoryType.Instruction, mem.MemoryType);
            Assert.Equal(new int[] { 2, 3, 4, 5, 6, 7, 8, 9 }, mem.Bytes);
            Assert.Equal("[Instruction: Bytes=[2, 3, 4, 5, 6, 7, 8, 9]]", mem.ToString());
        }

        [Fact]
        public void InstructionMemory_InitialisedConstructor_NineElements_ThrowsInvalidWordLengthException()
        {
            InstructionMemory mem;
            Assert.Throws<InvalidWordLengthException>( () => mem = new InstructionMemory(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }));
        }

        [Fact]
        public void InstructionMemory_ClearInstruction()
        {
            InstructionMemory mem = new InstructionMemory(new int[] { 2, 3, 4, 5, 6, 7, 8, 9 });
            Assert.Equal(MemoryType.Instruction, mem.MemoryType);
            Assert.Equal(new int[] { 2, 3, 4, 5, 6, 7, 8, 9 }, mem.Bytes);
            Assert.Equal("[Instruction: Bytes=[2, 3, 4, 5, 6, 7, 8, 9]]", mem.ToString());

            mem.ClearInstruction();

            Assert.Equal(InstructionMemory.NoOperation.Bytes, mem.Bytes);
            Assert.Equal("[Instruction: Bytes=[1, 1, 1, 1, 1, 1, 1, 1]]", mem.ToString());
        }
    }
}
