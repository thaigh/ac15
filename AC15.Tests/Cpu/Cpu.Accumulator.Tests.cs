using System;
using System.IO;
using AC15.Cpu;
using AC15.Exceptions;
using AC15.Memory;
using AC15.Registers;
using Xunit;

namespace AC15.Tests.Cpu
{
    public class AccumulatorCpuTests
    {
        #region SetValue

        [Fact]
        public void ACpu_Set0_Expect0()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(0);
            Assert.Equal(0, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void ACpu_Set10_Expect10()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            Assert.Equal(10, cpu.Accumulator.Value.GetFloatingPoint());
        }

        #endregion

        #region AddFunction

        [Fact]
        public void ACpu_Set10_Add5_Expect15()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            Assert.Equal(10, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Add(5);
            Assert.Equal(15, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void ACpu_Set10_AddN5_Expect5()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            Assert.Equal(10, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Add(-5);
            Assert.Equal(5, cpu.Accumulator.Value.GetFloatingPoint());
        }

        #endregion

        #region SubtractFunction

        [Fact]
        public void ACpu_Set10_Subtract5_Expect5()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            Assert.Equal(10, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Subtract(5);
            Assert.Equal(5, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void ACpu_Set10_SubtractN5_Expect15()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            Assert.Equal(10, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Subtract(-5);
            Assert.Equal(15, cpu.Accumulator.Value.GetFloatingPoint());
        }

        #endregion

        #region MultiplyFunction

        [Fact]
        public void ACpu_Set10_Multiply5_Expect50()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            Assert.Equal(10, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Multiply(5);
            Assert.Equal(50, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void ACpu_Set10_MultiplyN5_ExpectN50()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            Assert.Equal(10, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Multiply(-5);
            Assert.Equal(-50, cpu.Accumulator.Value.GetFloatingPoint());
        }

        #endregion

        #region DivideFunction

        [Fact]
        public void ACpu_Set10_Divide5_Expect2()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            Assert.Equal(10, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Divide(5);
            Assert.Equal(2, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void ACpu_Set9_Divide2_Expect4p5()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(9);
            Assert.Equal(9, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Divide(2);
            Assert.Equal(4.5, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void ACpu_Set10_DivideN5_ExpectN2()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            Assert.Equal(10, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Divide(-5);
            Assert.Equal(-2, cpu.Accumulator.Value.GetFloatingPoint());
        }

        #endregion

        #region PowerFunction

        [Fact]
        public void ACpu_Set2_Pow5_Expect32()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(2);
            Assert.Equal(2, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Power(5);
            Assert.Equal(32, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void ACpu_Set2_PowN2_ExpectOneQuarter()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(2);
            Assert.Equal(2, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Power(-2);
            Assert.Equal(0.25, cpu.Accumulator.Value.GetFloatingPoint());
        }

        #endregion

        #region SquareRootFunction

        [Fact]
        public void ACpu_Set4_Sqrt_Expect2()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(4);
            Assert.Equal(4, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Sqrt();
            Assert.Equal(2, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void ACpu_Set4_Root2_Expect2()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(4);
            Assert.Equal(4, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Root(2);
            Assert.Equal(2, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void ACpu_Set4_RootN2_ExpectOneHalf()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(4);
            Assert.Equal(4, cpu.Accumulator.Value.GetFloatingPoint());

            cpu.Root(-2);
            Assert.Equal(0.5, cpu.Accumulator.Value.GetFloatingPoint());
        }

        #endregion

        #region SetBoolean

        [Fact]
        public void ACpu_SetBoolean_False_ExpectFalse()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(false);
            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_SetBoolean_True_ExpectTrue()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(true);
            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region Negate

        [Fact]
        public void ACpu_Negate_False_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(false);
            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());

            cpu.Negate();
            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_Negate_True_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(true);
            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());

            cpu.Negate();
            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region BooleanEqualTo

        [Fact]
        public void ACpu_BooleanEqualTo_FalseFalse_ExpectTrue()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(false);

            cpu.EqualTo(false);
            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanEqualTo_FalseTrue_ExpectFalse()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(false);

            cpu.EqualTo(true);
            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanEqualTo_TrueFalse_ExpectFalse()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(true);

            cpu.EqualTo(false);
            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanEqualTo_TrueTrue_ExpectTrue()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(true);

            cpu.EqualTo(true);
            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region BooleanNotEqualTo

        [Fact]
        public void ACpu_BooleanNotEqualTo_FalseFalse_ExpectFalse()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(false);

            cpu.NotEqualTo(false);
            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanNotEqualTo_FalseTrue_ExpectTrue()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(false);

            cpu.NotEqualTo(true);
            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanNotEqualTo_TrueFalse_ExpectTrue()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(true);

            cpu.NotEqualTo(false);
            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanNotEqualTo_TrueTrue_ExpectFalse()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(true);

            cpu.NotEqualTo(true);
            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region BooleanAnd

        [Fact]
        public void ACpu_BooleanAnd_FalseFalse_ExpectFalse()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(false);

            cpu.And(false);
            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanAnd_FalseTrue_ExpectFalse()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(false);

            cpu.And(true);
            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanAnd_TrueFalse_ExpectFalse()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(true);

            cpu.And(false);
            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanAnd_TrueTrue_ExpectTrue()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(true);

            cpu.And(true);
            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region BooleanOr

        [Fact]
        public void ACpu_BooleanOr_FalseFalse_ExpectFalse()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(false);

            cpu.Or(false);
            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanOr_FalseTrue_ExpectTrue()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(false);

            cpu.Or(true);
            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanOr_TrueFalse_ExpectTrue()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(true);

            cpu.Or(false);
            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanOr_TrueTrue_ExpectTrue()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(true);

            cpu.Or(true);
            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region BooleanXor

        [Fact]
        public void ACpu_BooleanXor_FalseFalse_ExpectFalse()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(false);

            cpu.Xor(false);
            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanXor_FalseTrue_ExpectTrue()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(false);

            cpu.Xor(true);
            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanXor_TrueFalse_ExpectTrue()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(true);

            cpu.Xor(false);
            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_BooleanXor_TrueTrue_ExpectFalse()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(true);

            cpu.Xor(true);
            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region ReadFloat

        [Fact]
        public void ACpu_ReadFloat_ValidFloat_SetsAccumulatorTo10_123()
        {
            TextReader inputStream = new StringReader("10.123");

            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(0);
            cpu.InputStream = inputStream;

            cpu.ReadFloat();
            Assert.Equal(10.123, cpu.Accumulator.Value.GetFloatingPoint());

            inputStream.Close();
            inputStream.Dispose();
        }

        [Fact]
        public void ACpu_ReadFloat_ValidFloat_SetsAccumulatorTo10()
        {
            TextReader inputStream = new StringReader("10");

            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(0);
            cpu.InputStream = inputStream;

            cpu.ReadFloat();
            Assert.Equal(10.0, cpu.Accumulator.Value.GetFloatingPoint());

            inputStream.Close();
            inputStream.Dispose();
        }

        [Fact]
        public void ACpu_ReadFloat_InvalidFloat_ThrowsException()
        {
            TextReader inputStream = new StringReader("Invalid");

            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(0);
            cpu.InputStream = inputStream;

            IOStreamReadException exc = Assert.Throws<IOStreamReadException>(() => cpu.ReadFloat());
            Assert.Equal("Invalid", exc.TokenString);

            inputStream.Close();
            inputStream.Dispose();
        }

        #endregion

        #region ReadInteger

        [Fact]
        public void ACpu_ReadInteger_ValidFloat_ThrowsError()
        {
            TextReader inputStream = new StringReader("10.123");

            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(0);
            cpu.InputStream = inputStream;

            IOStreamReadException exc = Assert.Throws<IOStreamReadException>(() => cpu.ReadInteger());
            Assert.Equal("10.123", exc.TokenString);

            inputStream.Close();
            inputStream.Dispose();
        }

        [Fact]
        public void ACpu_ReadInteger_ValidInteger_SetsAccumulatorTo10()
        {
            TextReader inputStream = new StringReader("10");

            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(0);
            cpu.InputStream = inputStream;

            cpu.ReadInteger();
            Assert.Equal(10.0, cpu.Accumulator.Value.GetFloatingPoint());

            inputStream.Close();
            inputStream.Dispose();
        }

        [Fact]
        public void ACpu_ReadInteger_InvalidInteger_ThrowsException()
        {
            TextReader inputStream = new StringReader("Invalid");

            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(0);
            cpu.InputStream = inputStream;

            IOStreamReadException exc = Assert.Throws<IOStreamReadException>(() => cpu.ReadInteger());
            Assert.Equal("Invalid", exc.TokenString);

            inputStream.Close();
            inputStream.Dispose();
        }

        #endregion

        #region PrintAccumulatorValue

        [Fact]
        public void ACpu_PrintAccumulatorValue_Float()
        {
            TextWriter outputStream = new StringWriter();

            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            cpu.OutputStream = outputStream;

            cpu.PrintAccumulatorValue();

            Assert.Equal("10", outputStream.ToString());

            outputStream.Close();
            outputStream.Dispose();
        }

        [Fact]
        public void ACpu_PrintAccumulatorValue_Boolean()
        {
            TextWriter outputStream = new StringWriter();

            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(false);
            cpu.OutputStream = outputStream;

            cpu.PrintAccumulatorValue();

            Assert.Equal("False", outputStream.ToString());

            outputStream.Close();
            outputStream.Dispose();
        }

        #endregion

        #region PrintString

        [Fact]
        public void ACpu_PrintString()
        {
            TextWriter outputStream = new StringWriter();
            string message = "Hello World!";

            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            cpu.OutputStream = outputStream;

            cpu.PrintString(message);

            Assert.Equal(message, outputStream.ToString());

            outputStream.Close();
            outputStream.Dispose();
        }

        #endregion

        #region PrintChar

        [Fact]
        public void ACpu_PrintChar()
        {
            TextWriter outputStream = new StringWriter();
            char c = 'c';

            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            cpu.OutputStream = outputStream;

            cpu.PrintChar(c);

            Assert.Equal(1, outputStream.ToString().Length);
            Assert.Equal(c, outputStream.ToString()[0]);

            outputStream.Close();
            outputStream.Dispose();
        }

        #endregion

        #region PrintNewLine

        [Fact]
        public void ACpu_PrintNewLine()
        {
            TextWriter outputStream = new StringWriter();
            outputStream.NewLine = "\n";

            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            cpu.OutputStream = outputStream;

            cpu.PrintNewLine();

            Assert.Equal("\n", outputStream.ToString());

            outputStream.Close();
            outputStream.Dispose();
        }

        #endregion

        #region PrintSpace

        [Fact]
        public void ACpu_PrintSpace()
        {
            TextWriter outputStream = new StringWriter();

            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);
            cpu.OutputStream = outputStream;

            cpu.PrintSpace();

            Assert.Equal(" ", outputStream.ToString());

            outputStream.Close();
            outputStream.Dispose();
        }

        #endregion

        #region GreaterThan

        [Fact]
        public void ACpu_GreaterThan_0_0_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(0);

            cpu.GreaterThan();

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_GreaterThan_10_0_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.GreaterThan();

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_GreaterThan_10_9_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.GreaterThan(9);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_GreaterThan_10_10_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.GreaterThan(10);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_GreaterThan_10_11_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.GreaterThan(11);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region GreaterThanOrEqual

        [Fact]
        public void ACpu_GreaterThanOrEqual_0_0_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(0);

            cpu.GreaterThanOrEqual();

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_GreaterThanOrEqual_10_0_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.GreaterThanOrEqual();

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_GreaterThanOrEqual_10_9_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.GreaterThanOrEqual(9);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_GreaterThanOrEqual_10_10_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.GreaterThanOrEqual(10);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_GreaterThanOrEqual_10_11_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.GreaterThanOrEqual(11);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region LessThan

        [Fact]
        public void ACpu_LessThan_0_0_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(0);

            cpu.LessThan();

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_LessThan_10_0_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.LessThan();

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }
        
        [Fact]
        public void ACpu_LessThan_10_9_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.LessThan(9);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_LessThan_10_10_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.LessThan(10);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_LessThan_10_11_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.LessThan(11);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region LessThanOrEqual

        [Fact]
        public void ACpu_LessThanOrEqual_0_0_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(0);

            cpu.LessThanOrEqual();

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_LessThanOrEqual_10_0_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.LessThanOrEqual();

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }
        
        [Fact]
        public void ACpu_LessThanOrEqual_10_9_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.LessThanOrEqual(9);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_LessThanOrEqual_10_10_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.LessThanOrEqual(10);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_LessThanOrEqual_10_11_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.LessThanOrEqual(11);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region EqualTo

        [Fact]
        public void ACpu_EqualTo_0_0_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(0);

            cpu.EqualTo();

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_EqualTo_10_0_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.EqualTo();

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }
        
        [Fact]
        public void ACpu_EqualTo_10_9_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.EqualTo(9);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_EqualTo_10_10_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.EqualTo(10);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region NotEqualTo

        [Fact]
        public void ACpu_NotEqualTo_0_0_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(0);

            cpu.NotEqualTo();

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_NotEqualTo_10_0_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.NotEqualTo();

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }
        
        [Fact]
        public void ACpu_NotEqualTo_10_9_True()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.NotEqualTo(9);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void ACpu_NotEqualTo_10_10_False()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.Set(10);

            cpu.NotEqualTo(10);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region ResetIndex

        [Fact]
        public void ACpu_ResetIndex()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.ArrayIndexRegister.ArrayIndex = 10;
            Assert.Equal(10, cpu.ArrayIndexRegister.ArrayIndex);

            cpu.ResetIndex();

            Assert.Equal(0, cpu.ArrayIndexRegister.ArrayIndex);
        }

        #endregion

        #region SetIndex

        [Fact]
        public void ACpu_SetIndex()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.ArrayIndexRegister.ArrayIndex = 10;
            Assert.Equal(10, cpu.ArrayIndexRegister.ArrayIndex);

            cpu.SetIndex(5);

            Assert.Equal(5, cpu.ArrayIndexRegister.ArrayIndex);
        }

        #endregion

        #region IncrementIndex

        [Fact]
        public void ACpu_IncrementIndex()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.ArrayIndexRegister.ArrayIndex = 10;
            Assert.Equal(10, cpu.ArrayIndexRegister.ArrayIndex);

            cpu.IncrementIndex();

            Assert.Equal(11, cpu.ArrayIndexRegister.ArrayIndex);
        }

        #endregion

        #region DecrementIndex

        [Fact]
        public void ACpu_DecrementIndex()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.ArrayIndexRegister.ArrayIndex = 10;
            Assert.Equal(10, cpu.ArrayIndexRegister.ArrayIndex);

            cpu.DecrementIndex();

            Assert.Equal(9, cpu.ArrayIndexRegister.ArrayIndex);
        }

        #endregion

        #region LoadIndexFromAddress

        [Fact]
        public void ACpu_LoadIndexFromAddress()
        {
            Ram ram = new Ram();
            ram.AddFloatingPoint(12);

            RegisterGroup registers = new RegisterGroup(ram, new InstructionSet(), new ArrayDescriptorRegister(), new SubprogramDescriptorRegister());
            
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.LoadRegisterGroup(registers);

            cpu.ArrayIndexRegister.ArrayIndex = 10;
            Assert.Equal(10, cpu.ArrayIndexRegister.ArrayIndex);

            cpu.LoadIndexFromAddress(0);

            Assert.Equal(12, cpu.ArrayIndexRegister.ArrayIndex);
        }

        #endregion

        #region LoadIndexFromAccumulator

        [Fact]
        public void ACpu_LoadIndexFromAccumulator()
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.ArrayIndexRegister.ArrayIndex = 10;
            Assert.Equal(10, cpu.ArrayIndexRegister.ArrayIndex);

            cpu.Set(12);
            cpu.LoadIndexFromAccumulator();

            Assert.Equal(12, cpu.ArrayIndexRegister.ArrayIndex);
        }

        #endregion

    }
}
