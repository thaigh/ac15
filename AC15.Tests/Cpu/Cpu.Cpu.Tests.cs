﻿using System;
using Xunit;
using AC15.Cpu;
using AC15.Instructions;

namespace AC15.Tests.Cpu
{
    public class CpuTests
    {
        private const string ProgramDataFile = @"
            WORD 0.0
            WORD 1.0
            WORD 2.0
            WORD 3.0
            WORD 4.0
            WORD 5.0
            WORD 6.0
            WORD 7.0
            WORD 8.0
            WORD 9.0
            WORD 10.0";

        AccumulatorCpu cpu = new AccumulatorCpu();
        public CpuTests()
        {
            cpu = new AccumulatorCpu();
            cpu.LoadProgramFromText(ProgramDataFile);
        }

        [Fact]
        public void Cpu_Decode()
        {
            cpu.Set(10);
            cpu.OpCodeRegister.OpCode = (int)OpCode.Add;
            cpu.OpCodeRegister.Operand = 10;
            cpu.Execute(cpu.Decode(OpCode.Add));
            Assert.Equal(20, cpu.Accumulator.Value.GetFloatingPoint());
        }

        //[Fact]
        //public void Cpu_Cycle()
        //{
        //    AccumulatorCpu cpu = new AccumulatorCpu();
        //    cpu.Set(10);
        //    cpu.OpCodeRegister.OpCode = (int)OpCode.Add;
        //    cpu.OpCodeRegister.Operand = 10;
        //    cpu.Cycle();
        //    Assert.Equal(20, cpu.Accumulator.Value);
        //}
    }
}
