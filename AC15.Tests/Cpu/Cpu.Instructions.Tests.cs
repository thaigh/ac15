﻿using AC15.Cpu;
using AC15.Exceptions;
using AC15.Instructions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;

namespace AC15.Tests.Cpu
{
    public class CpuInstructionTests
    {

        private const string ProgramDataFile = @"
            WORD 0.0
            WORD 1.0
            WORD 2.0
            WORD 3.0
            WORD 4.0
            WORD 5.0
            WORD 6.0
            WORD 7.0
            WORD 8.0
            WORD 9.0
            WORD 10.0";

        AccumulatorCpu cpu = new AccumulatorCpu();

        public CpuInstructionTests()
        {
            cpu = new AccumulatorCpu();
            cpu.LoadProgramFromText(ProgramDataFile);
            cpu.Set(10);
        }

        #region Math

        [Fact]
        public void Cpu_Add5_Total15()
        {
            AddInstruction inst = new AddInstruction(5);
            inst.Execute(cpu);
            Assert.Equal(15, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void Cpu_Subtract5_Total5()
        {
            SubtractInstruction inst = new SubtractInstruction(5);
            inst.Execute(cpu);
            Assert.Equal(5, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void Cpu_Multiply5_Total50()
        {
            MultiplyInstruction inst = new MultiplyInstruction(5);
            inst.Execute(cpu);
            Assert.Equal(50, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void Cpu_Divide5_Total2()
        {
            DivideInstruction inst = new DivideInstruction(5);
            inst.Execute(cpu);
            Assert.Equal(2, cpu.Accumulator.Value.GetFloatingPoint());
        }

        #endregion

        #region Address

        [Fact]
        public void Cpu_Get3_Acc3()
        {
            GetInstruction inst = new GetInstruction(3);
            inst.Execute(cpu);
            Assert.Equal(3, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void Cpu_Store3_Address3Equals10()
        {
            StoreInstruction inst = new StoreInstruction(3);
            inst.Execute(cpu);
            Assert.Equal(10, cpu.RegisterGroup.Ram.GetFloatingPoint(3).Value);
        }

        #endregion

        #region Io

        [Fact]
        public void Cpu_ReadFloat_AccSetTo10()
        {
            TextReader inputStream = new StringReader("10.123");

            cpu.Set(0);
            cpu.InputStream = inputStream;

            ReadFloatInstruction inst = new ReadFloatInstruction();
            inst.Execute(cpu);

            Assert.Equal(10.123, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void Cpu_ReadInteger_AccSetTo10()
        {
            TextReader inputStream = new StringReader("10");

            cpu.Set(0);
            cpu.InputStream = inputStream;

            ReadIntegerInstruction inst = new ReadIntegerInstruction();
            inst.Execute(cpu);

            Assert.Equal(10, cpu.Accumulator.Value.GetFloatingPoint());
        }

        [Fact]
        public void Cpu_ValuePrint_WritesAccumulatorValueToStream()
        {
            StringWriter stream = new StringWriter();
            cpu.OutputStream = stream;
            cpu.Set(13);

            ValuePrintInstruction inst = new ValuePrintInstruction();
            inst.Execute(cpu);

            string actual = stream.GetStringBuilder().ToString();
            Assert.Equal("13", actual);
        }

        [Fact]
        public void Cpu_StringPrint_WritesStringToStream()
        {
            StringWriter stream = new StringWriter();
            cpu.OutputStream = stream;

            StringPrintInstruction inst = new StringPrintInstruction("Hello Wo");
            inst.Execute(cpu);

            string actual = stream.GetStringBuilder().ToString();
            Assert.Equal("Hello Wo", actual);
        }

        [Fact]
        public void Cpu_NewLine_WritesNewLineToStream()
        {
            StringWriter stream = new StringWriter();
            cpu.OutputStream = stream;

            NewLineInstruction inst = new NewLineInstruction();
            inst.Execute(cpu);

            string actual = stream.GetStringBuilder().ToString();
            Assert.Equal(Environment.NewLine, actual);
        }

        [Fact]
        public void Cpu_SpacePrint_WritesSpaceToStream()
        {
            StringWriter stream = new StringWriter();
            cpu.OutputStream = stream;

            SpacePrintInstruction inst = new SpacePrintInstruction();
            inst.Execute(cpu);

            string actual = stream.GetStringBuilder().ToString();
            Assert.Equal(" ", actual);
        }

        #endregion

        #region BooleanLogic

        [Fact]
        public void Cpu_BooleanNegate_NonBooleanAccumulatorValue_ThrowsException()
        {
            cpu.Set(10);
            NegateInstruction inst = new NegateInstruction();
            Assert.Throws<InvalidMemoryCastException>(() => inst.Execute(cpu) );
        }


        [Fact]
        public void Cpu_BooleanNegate_NegatesAccumulator()
        {
            cpu.Set(false);

            NegateInstruction inst = new NegateInstruction();
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_BooleanEqualTo_FalseFalse_AccSetToTrue()
        {
            cpu.Set(false);

            BooleanEqualToInstruction inst = new BooleanEqualToInstruction(false);
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_BooleanEqualTo_FalseTrue_AccSetToFalse()
        {
            cpu.Set(false);

            BooleanEqualToInstruction inst = new BooleanEqualToInstruction(true);
            inst.Execute(cpu);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_BooleanNotEqualTo_FalseFalse_AccSetToFalse()
        {
            cpu.Set(false);

            BooleanNotEqualToInstruction inst = new BooleanNotEqualToInstruction(false);
            inst.Execute(cpu);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_BooleanNotEqualTo_FalseTrue_AccSetToTrue()
        {
            cpu.Set(false);

            BooleanNotEqualToInstruction inst = new BooleanNotEqualToInstruction(true);
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_BooleanAnd_FalseFalse_AccSetToFalse()
        {
            cpu.Set(false);

            BooleanAndInstruction inst = new BooleanAndInstruction(false);
            inst.Execute(cpu);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_BooleanAnd_TrueTrue_AccSetToTrue()
        {
            cpu.Set(true);

            BooleanAndInstruction inst = new BooleanAndInstruction(true);
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_BooleanOr_FalseFalse_AccSetToFalse()
        {
            cpu.Set(false);

            BooleanOrInstruction inst = new BooleanOrInstruction(false);
            inst.Execute(cpu);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_BooleanOr_FalseTrue_AccSetToTrue()
        {
            cpu.Set(false);

            BooleanOrInstruction inst = new BooleanOrInstruction(true);
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_BooleanXor_FalseFalse_AccSetToFalse()
        {
            cpu.Set(false);

            BooleanXorInstruction inst = new BooleanXorInstruction(false);
            inst.Execute(cpu);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_BooleanXor_FalseTrue_AccSetToTrue()
        {
            cpu.Set(false);

            BooleanXorInstruction inst = new BooleanXorInstruction(true);
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region MathBoolean

        [Fact]
        public void Cpu_GreaterThan_NonFloatingPointAccumulatorValue_ThrowsException()
        {
            cpu.Set(false);
            GreaterThanInstruction inst = new GreaterThanInstruction();
            Assert.Throws<InvalidMemoryCastException>(() => inst.Execute(cpu) );
        }

        [Fact]
        public void Cpu_GreaterThan_N1_SetAccToFalse()
        {
            cpu.Set(-1);

            GreaterThanInstruction inst = new GreaterThanInstruction();
            inst.Execute(cpu);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_GreaterThan_0_SetAccToFalse()
        {
            cpu.Set(0);

            GreaterThanInstruction inst = new GreaterThanInstruction();
            inst.Execute(cpu);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_GreaterThan_1_SetAccToTrue()
        {
            cpu.Set(1);

            GreaterThanInstruction inst = new GreaterThanInstruction();
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }



        [Fact]
        public void Cpu_GreaterThanOrEqual_NonFloatingPointAccumulatorValue_ThrowsException()
        {
            cpu.Set(false);
            GreaterThanOrEqualInstruction inst = new GreaterThanOrEqualInstruction();
            Assert.Throws<InvalidMemoryCastException>(() => inst.Execute(cpu) );
        }

        [Fact]
        public void Cpu_GreaterThanOrEqual_N1_SetAccToFalse()
        {
            cpu.Set(-1);

            GreaterThanOrEqualInstruction inst = new GreaterThanOrEqualInstruction();
            inst.Execute(cpu);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_GreaterThanOrEqual_0_SetAccToTrue()
        {
            cpu.Set(0);

            GreaterThanOrEqualInstruction inst = new GreaterThanOrEqualInstruction();
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_GreaterThanOrEqual_1_SetAccToTrue()
        {
            cpu.Set(1);

            GreaterThanOrEqualInstruction inst = new GreaterThanOrEqualInstruction();
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }



        [Fact]
        public void Cpu_LessThan_NonFloatingPointAccumulatorValue_ThrowsException()
        {
            cpu.Set(false);
            LessThanInstruction inst = new LessThanInstruction();
            Assert.Throws<InvalidMemoryCastException>(() => inst.Execute(cpu) );
        }

        [Fact]
        public void Cpu_LessThan_N1_SetAccToTrue()
        {
            cpu.Set(-1);

            LessThanInstruction inst = new LessThanInstruction();
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_LessThan_0_SetAccToFalse()
        {
            cpu.Set(0);

            LessThanInstruction inst = new LessThanInstruction();
            inst.Execute(cpu);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_LessThan_1_SetAccToFalse()
        {
            cpu.Set(1);

            LessThanInstruction inst = new LessThanInstruction();
            inst.Execute(cpu);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }



        [Fact]
        public void Cpu_LessThanOrEqual_NonFloatingPointAccumulatorValue_ThrowsException()
        {
            cpu.Set(false);
            LessThanOrEqualInstruction inst = new LessThanOrEqualInstruction();
            Assert.Throws<InvalidMemoryCastException>(() => inst.Execute(cpu) );
        }

        [Fact]
        public void Cpu_LessThanOrEqual_N1_SetAccToTrue()
        {
            cpu.Set(-1);

            LessThanOrEqualInstruction inst = new LessThanOrEqualInstruction();
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_LessThanOrEqual_0_SetAccToTrue()
        {
            cpu.Set(0);

            LessThanOrEqualInstruction inst = new LessThanOrEqualInstruction();
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_LessThanOrEqual_1_SetAccToFalse()
        {
            cpu.Set(1);

            LessThanOrEqualInstruction inst = new LessThanOrEqualInstruction();
            inst.Execute(cpu);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }



        [Fact]
        public void Cpu_EqualTo_NonFloatingPointAccumulatorValue_ThrowsException()
        {
            cpu.Set(false);
            EqualToInstruction inst = new EqualToInstruction();
            Assert.Throws<InvalidMemoryCastException>(() => inst.Execute(cpu) );
        }

        [Fact]
        public void Cpu_EqualTo_N1_SetAccToFalse()
        {
            cpu.Set(-1);

            EqualToInstruction inst = new EqualToInstruction();
            inst.Execute(cpu);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_EqualTo_0_SetAccToTrue()
        {
            cpu.Set(0);

            EqualToInstruction inst = new EqualToInstruction();
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }



        [Fact]
        public void Cpu_NotEqualTo_NonFloatingPointAccumulatorValue_ThrowsException()
        {
            cpu.Set(false);
            NotEqualToInstruction inst = new NotEqualToInstruction();
            Assert.Throws<InvalidMemoryCastException>(() => inst.Execute(cpu) );
        }

        [Fact]
        public void Cpu_NotEqualTo_N1_SetAccToTrue()
        {
            cpu.Set(-1);

            NotEqualToInstruction inst = new NotEqualToInstruction();
            inst.Execute(cpu);

            Assert.Equal(true, cpu.Accumulator.Value.GetBoolean());
        }

        [Fact]
        public void Cpu_NotEqualTo_0_SetAccToFalse()
        {
            cpu.Set(0);

            NotEqualToInstruction inst = new NotEqualToInstruction();
            inst.Execute(cpu);

            Assert.Equal(false, cpu.Accumulator.Value.GetBoolean());
        }

        #endregion

        #region ArrayMachine

        [Fact]
        public void Cpu_AccumulatorToIndex_10_SetIdxTo10()
        {
            cpu.ArrayIndexRegister.ArrayIndex = 0;
            Assert.Equal(0, cpu.ArrayIndexRegister.ArrayIndex);

            cpu.Set(10);
            AccumulatorToIndexInstruction inst = new AccumulatorToIndexInstruction();
            inst.Execute(cpu);

            Assert.Equal(10, cpu.ArrayIndexRegister.ArrayIndex);
        }

        [Fact]
        public void Cpu_IncrementIndex()
        {
            cpu.ArrayIndexRegister.ArrayIndex = 0;
            Assert.Equal(0, cpu.ArrayIndexRegister.ArrayIndex);

            IncrementIndexInstruction inst = new IncrementIndexInstruction();
            inst.Execute(cpu);

            Assert.Equal(1, cpu.ArrayIndexRegister.ArrayIndex);
        }

        [Fact]
        public void Cpu_DecrementIndex()
        {
            cpu.ArrayIndexRegister.ArrayIndex = 0;
            Assert.Equal(0, cpu.ArrayIndexRegister.ArrayIndex);

            DecrementIndexInstruction inst = new DecrementIndexInstruction();
            inst.Execute(cpu);

            Assert.Equal(-1, cpu.ArrayIndexRegister.ArrayIndex);
        }


        [Fact]
        public void Cpu_ResetIndex_10_SetIdxTo0()
        {
            cpu.ArrayIndexRegister.ArrayIndex = 10;
            Assert.Equal(10, cpu.ArrayIndexRegister.ArrayIndex);

            ResetIndexInstruction inst = new ResetIndexInstruction();
            inst.Execute(cpu);

            Assert.Equal(0, cpu.ArrayIndexRegister.ArrayIndex);
        }


        [Fact]
        public void Cpu_LoadIndexInstruction_10_SetIdxTo0()
        {
            cpu.ArrayIndexRegister.ArrayIndex = 10;
            Assert.Equal(10, cpu.ArrayIndexRegister.ArrayIndex);

            LoadIndexInstruction inst = new LoadIndexInstruction(2);
            inst.Execute(cpu);

            Assert.Equal(2, cpu.ArrayIndexRegister.ArrayIndex);
        }

        #endregion

    }
}
