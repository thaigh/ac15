﻿using System;
using Xunit;
using AC15.File;
using AC15.Registers;
using AC15.Memory;
using AC15.Exceptions;
namespace AC15.Tests.File
{
    public class CodeModuleReaderTests
    {

        #region Word

        [Fact]
        public void ReadCode_Word2_0()
        {
            string code = "WORD 2.0";
            CodeModuleReader reader = new CodeModuleReader();
            RegisterGroup rg = reader.LoadCodeModuleFromText(code);

            Assert.Equal(1, rg.Ram.Size);
            Assert.IsType<FloatingPointMemory>(rg.Ram.Get(0));

            FloatingPointMemory fp = (FloatingPointMemory)rg.Ram.Get(0);
            Assert.Equal(2.0, fp.Value);
        }

        [Fact]
        public void ReadCode_WordN2()
        {
            string code = "WORD -2.0";
            CodeModuleReader reader = new CodeModuleReader();
            RegisterGroup rg = reader.LoadCodeModuleFromText(code);

            Assert.Equal(1, rg.Ram.Size);
            Assert.IsType<FloatingPointMemory>(rg.Ram.Get(0));

            FloatingPointMemory fp = (FloatingPointMemory)rg.Ram.Get(0);
            Assert.Equal(-2.0, fp.Value);
        }

        [Fact]
        public void ReadCode_WordN2_5()
        {
            string code = "WORD -2.5";
            CodeModuleReader reader = new CodeModuleReader();
            RegisterGroup rg = reader.LoadCodeModuleFromText(code);

            Assert.Equal(1, rg.Ram.Size);
            Assert.IsType<FloatingPointMemory>(rg.Ram.Get(0));

            FloatingPointMemory fp = (FloatingPointMemory)rg.Ram.Get(0);
            Assert.Equal(-2.5, fp.Value);
        }

        [Fact]
        public void ReadCode_WordN2_2()
        {
            string code = "WORD -2.2";
            CodeModuleReader reader = new CodeModuleReader();
            RegisterGroup rg = reader.LoadCodeModuleFromText(code);

            Assert.Equal(1, rg.Ram.Size);
            Assert.IsType<FloatingPointMemory>(rg.Ram.Get(0));

            FloatingPointMemory fp = (FloatingPointMemory)rg.Ram.Get(0);
            Assert.Equal(float.Parse("-2.2"), fp.Value);
            Assert.Equal(-2.2, fp.Value, 5); // IMPROVE: Floating Point Precision?
        }

        [Fact]
        public void ReadCode_Word2()
        {
            string code = "WORD 2";
            CodeModuleReader reader = new CodeModuleReader();
            RegisterGroup rg = reader.LoadCodeModuleFromText(code);

            Assert.Equal(1, rg.Ram.Size);
            Assert.IsType<FloatingPointMemory>(rg.Ram.Get(0));

            FloatingPointMemory fp = (FloatingPointMemory)rg.Ram.Get(0);
            Assert.Equal(2, fp.Value);
        }

        #endregion

        #region Block

        [Fact]
        public void ReadCode_Block_0_ThrowsException()
        {
            string code = "BLOCK 0";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }

        [Fact]
        public void ReadCode_BlockN1_ThrowsException()
        {
            string code = "BLOCK -1";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }

        [Fact]
        public void ReadCode_Block2()
        {
            string code = "BLOCK 2";
            CodeModuleReader reader = new CodeModuleReader();
            RegisterGroup rg = reader.LoadCodeModuleFromText(code);

            Assert.Equal(2, rg.Ram.Size);
            Assert.IsType<FloatingPointMemory>(rg.Ram.Get(0));

            FloatingPointMemory fp = (FloatingPointMemory)rg.Ram.Get(0);
            Assert.Equal(0, fp.Value);

            Assert.Equal(1, rg.ArrayDescriptors.Size);
            ArrayDescriptor desc = rg.ArrayDescriptors.Get(0);
            Assert.Equal(2, desc.ArraySize);
            Assert.Equal(0, desc.ArrayStartAddress);
        }

        [Fact]
        public void ReadCode_Block2_2()
        {
            string code = "BLOCK 2.2";
            CodeModuleReader reader = new CodeModuleReader();
            RegisterGroup rg = reader.LoadCodeModuleFromText(code);

            Assert.Equal(2, rg.Ram.Size);
            Assert.IsType<FloatingPointMemory>(rg.Ram.Get(0));

            FloatingPointMemory fp = (FloatingPointMemory)rg.Ram.Get(0);
            Assert.Equal(0, fp.Value);

            Assert.Equal(1, rg.ArrayDescriptors.Size);
            ArrayDescriptor desc = rg.ArrayDescriptors.Get(0);
            Assert.Equal(2, desc.ArraySize);
            Assert.Equal(0, desc.ArrayStartAddress);
        }

        #endregion

        #region String

        [Fact]
        public void ReadCode_StringN1_ThrowsException()
        {
            string code = "STRING -1";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }

        [Fact]
        public void ReadCode_String0_ThrowsException()
        {
            string code = "STRING 0";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }

        [Fact]
        public void ReadCode_String1_Valid_ABCDEFGH()
        {
            string code = "STRING 1\n   65 66 67 68    69 70 71 72";
            CodeModuleReader reader = new CodeModuleReader();
            RegisterGroup rg = reader.LoadCodeModuleFromText(code);

            Assert.Equal(1, rg.Ram.Size);
            Assert.IsType<StringMemory>(rg.Ram.Get(0));

            StringMemory mem = (StringMemory)rg.Ram.Get(0);
            Assert.Equal(new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' }, mem.Characters);
        }

        [Fact]
        public void ReadCode_String1_InvalidLengthNonExist_ThrowsException()
        {
            string code = "STRING 1";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }

        [Fact]
        public void ReadCode_String1_InvalidLength0_ThrowsException()
        {
            string code = "STRING 1\n ";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }

        [Fact]
        public void ReadCode_String1_InvalidLength7_ThrowsException()
        {
            string code = "STRING 1\n   65 66 67 68    69 70 71";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }

        [Fact]
        public void ReadCode_String1_InvalidLength9_ThrowsException()
        {
            string code = "STRING 1\n   65 66 67 68    69 70 71 72    73";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }


        #endregion

        #region Code

        [Fact]
        public void ReadCode_CodeN1_ThrowsException()
        {
            string code = "CODE -1";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }

        [Fact]
        public void ReadCode_Code0_ThrowsException()
        {
            string code = "CODE 0";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }

        [Fact]
        public void ReadCode_Code1_InvalidLengthNonExist_ThrowsException()
        {
            string code = "CODE 1";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }

        [Fact]
        public void ReadCode_Code1_InvalidLength0_ThrowsException()
        {
            string code = "CODE 1\n ";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }

        [Fact]
        public void ReadCode_Code1_InvalidLength7_ThrowsException()
        {
            string code = "CODE 1\n   65 66 67 68    69 70 71";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }

        [Fact]
        public void ReadCode_Code1_InvalidLength9_ThrowsException()
        {
            string code = "CODE 1\n   65 66 67 68    69 70 71 72    73";
            CodeModuleReader reader = new CodeModuleReader();
            Assert.Throws<CodeModuleException>(() => reader.LoadCodeModuleFromText(code));
        }

        [Fact]
        public void ReadCode_Code1_Valid_12345678()
        {
            string code = "CODE 1\n   1 2 3 4 5 6 7 8";
            CodeModuleReader reader = new CodeModuleReader();
            RegisterGroup rg = reader.LoadCodeModuleFromText(code);

            Assert.Equal(1, rg.SubprogramDescriptors.Size);
            SubprogramDescriptor desc = rg.SubprogramDescriptors.Get(0);
            Assert.Equal(0, desc.EntryAddress);
            Assert.Equal(0, desc.ReturnAddress);

            Assert.Equal(1, rg.Instructions.Size);
            InstructionMemory mem = rg.Instructions.Get(0);
            Assert.Equal(new int[] { 1,2,3,4,5,6,7,8 }, mem.Bytes);
        }

        #endregion

    }
}
