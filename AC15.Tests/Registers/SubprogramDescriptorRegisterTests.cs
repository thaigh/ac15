﻿using System;
using AC15.Exceptions;
using AC15.Memory;
using AC15.Registers;
using Xunit;

namespace AC15.Tests.Registers
{
    public class SubprogramDescriptorRegisterTests
    {
        [Fact]
        public void Add_IncreasesSize_GetAtIndex0()
        {
            SubprogramDescriptorRegister reg = new SubprogramDescriptorRegister();
            reg.Add(new SubprogramDescriptor(1, 2));
            Assert.Equal(1, reg.Size);
            Assert.Equal(1, reg.Get(0).EntryAddress);
            Assert.Equal(2, reg.Get(0).ReturnAddress);
        }

        [Fact]
        public void Add_ExceedsLimit_ThrowsException()
        {
            SubprogramDescriptorRegister reg = new SubprogramDescriptorRegister();
            for (int i = 0; i < SubprogramDescriptorRegister.MaxSubProgramDescriptors; i++)
            {
                reg.Add(new SubprogramDescriptor(1, 1));
            }

            Assert.Equal(SubprogramDescriptorRegister.MaxSubProgramDescriptors, reg.Size);
            Assert.Throws<RegisterLimitException>(() => reg.Add(new SubprogramDescriptor(1, 1)));
        }

        [Fact]
        public void Get_NoItems_N1_ThrowsEmptyRegisterException()
        {
            SubprogramDescriptorRegister reg = new SubprogramDescriptorRegister();
            Assert.Equal(0, reg.Size);
            Assert.Throws<EmptyRegisterException>(() => reg.Get(-1));
        }

        [Fact]
        public void Get_NoItems_0_ThrowsEmptyRegisterException()
        {
            SubprogramDescriptorRegister reg = new SubprogramDescriptorRegister();
            Assert.Equal(0, reg.Size);
            Assert.Throws<EmptyRegisterException>(() => reg.Get(0));
        }

        [Fact]
        public void Get_OneItem_N1_ThrowsAddressOutOfBoundsException()
        {
            SubprogramDescriptorRegister reg = new SubprogramDescriptorRegister();
            reg.Add(new SubprogramDescriptor(1, 1));
            Assert.Equal(1, reg.Size);
            Assert.Throws<AddressOutOfBoundsException>(() => reg.Get(-1));
        }

        [Fact]
        public void Get_OneItem_0_ReturnsItem()
        {
            SubprogramDescriptorRegister reg = new SubprogramDescriptorRegister();
            reg.Add(new SubprogramDescriptor(1, 2));
            Assert.Equal(1, reg.Size);
            Assert.Equal(1, reg.Get(0).EntryAddress);
            Assert.Equal(2, reg.Get(0).ReturnAddress);
        }

        [Fact]
        public void Get_OneItem_1_ThrowsAddressOutOfBoundsException()
        {
            SubprogramDescriptorRegister reg = new SubprogramDescriptorRegister();
            reg.Add(new SubprogramDescriptor(1, 1));
            Assert.Equal(1, reg.Size);
            Assert.Throws<AddressOutOfBoundsException>(() => reg.Get(1));
        }

        [Fact]
        public void Set_NoItems_N1_ThrowsEmptyRegisterException()
        {
            SubprogramDescriptorRegister reg = new SubprogramDescriptorRegister();
            Assert.Equal(0, reg.Size);
            Assert.Throws<EmptyRegisterException>(() => reg.Set(-1, new SubprogramDescriptor(1, 1)));
        }

        [Fact]
        public void Set_NoItems_0_ThrowsEmptyRegisterException()
        {
            SubprogramDescriptorRegister reg = new SubprogramDescriptorRegister();
            Assert.Equal(0, reg.Size);
            Assert.Throws<EmptyRegisterException>(() => reg.Set(0, new SubprogramDescriptor(1, 1)));
        }

        [Fact]
        public void Set_OneItem_N1_ThrowsAddressOutOfBoundsException()
        {
            SubprogramDescriptorRegister reg = new SubprogramDescriptorRegister();
            reg.Add(new SubprogramDescriptor(1, 1));
            Assert.Equal(1, reg.Size);
            Assert.Throws<AddressOutOfBoundsException>(() => reg.Set(-1, new SubprogramDescriptor(1, 1)));
        }

        [Fact]
        public void Set_OneItem_0_SetsNewValue()
        {
            SubprogramDescriptorRegister reg = new SubprogramDescriptorRegister();
            reg.Add(new SubprogramDescriptor(1, 2));

            Assert.Equal(1, reg.Size);
            Assert.Equal(1, reg.Get(0).EntryAddress);
            Assert.Equal(2, reg.Get(0).ReturnAddress);

            reg.Set(0, new SubprogramDescriptor(3, 4));

            Assert.Equal(3, reg.Get(0).EntryAddress);
            Assert.Equal(4, reg.Get(0).ReturnAddress);
        }

        [Fact]
        public void Set_OneItem_1_ThrowsAddressOutOfBoundsException()
        {
            SubprogramDescriptorRegister reg = new SubprogramDescriptorRegister();
            reg.Add(new SubprogramDescriptor(1, 1));
            Assert.Equal(1, reg.Size);
            Assert.Throws<AddressOutOfBoundsException>(() => reg.Set(1, new SubprogramDescriptor(1, 1)));
        }
        
    }
}
