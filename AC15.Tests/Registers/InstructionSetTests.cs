﻿using System;
using Xunit;
using AC15.Registers;
using AC15.Memory;
using AC15.Exceptions;

namespace AC15.Tests.Registers
{
    public class InstructionSetTests
    {

        #region Get

        [Fact]
        public void InstructionSet_EmptyRegister_GetN1_ThrowsEmptyRegisterException()
        {
            InstructionSet i = new InstructionSet();
            Assert.Throws<EmptyRegisterException>(() => i.Get(-1));
        }

        [Fact]
        public void InstructionSet_EmptyRegister_Get0_ThrowsEmptyRegisterException()
        {
            InstructionSet i = new InstructionSet();
            Assert.Throws<EmptyRegisterException>(() => i.Get(1));
        }

        [Fact]
        public void InstructionSet_OneItem_GetN1_ThrowsAddressOutOfBoundsException()
        {
            InstructionSet i = new InstructionSet();
            i.Add(new InstructionMemory());
            Assert.Throws<AddressOutOfBoundsException>(() => i.Get(-1));
        }

        [Fact]
        public void InstructionSet_OneItem_Get0_ReturnsInstruction()
        {
            InstructionSet i = new InstructionSet();
            i.Add(new InstructionMemory(new int[] { 1 }));
            InstructionMemory mem = i.Get(0);
            Assert.Equal(mem.MemoryType, MemoryType.Instruction);
            Assert.Equal(InstructionMemory.NoOperation.Bytes, mem.Bytes);
        }

        [Fact]
        public void InstructionSet_OneItem_Get1_ThrowsAddressOutOfBoundsException()
        {
            InstructionSet i = new InstructionSet();
            i.Add(new InstructionMemory());
            Assert.Throws<AddressOutOfBoundsException>(() => i.Get(1));
        }

        #endregion

        #region Set

        [Fact]
        public void InstructionSet_EmptyRegister_SetN1_ThrowsException()
        {
            InstructionSet i = new InstructionSet();
            Assert.Throws<EmptyRegisterException>(() => i.Set(-1, new int[] { 1 }));
        }

        [Fact]
        public void InstructionSet_EmptyRegister_Set0_ThrowsException()
        {
            InstructionSet i = new InstructionSet();
            Assert.Throws<EmptyRegisterException>(() => i.Set(0, new int[] { 1 }));
        }

        [Fact]
        public void InstructionSet_OneItem_SetN1_ThrowsException()
        {
            InstructionSet i = new InstructionSet();
            i.Add(new InstructionMemory(new int[] { 5 }));
            Assert.Throws<AddressOutOfBoundsException>(() => i.Set(-1, new int[] { 1 }));
        }

        [Fact]
        public void InstructionSet_OneItem_Set0_SetsInstruction()
        {
            InstructionSet i = new InstructionSet();
            i.Add(new InstructionMemory(new int[] { 5 }));
            Assert.Equal(5, i[0].Bytes[0]);

            // Test set int[]
            i.Set(0, new int[] { 1 });
            Assert.Equal(1, i[0].Bytes[0]);

            // Test set InstructionMemory
            i.Set(0, new InstructionMemory(new int[] { 2 }));
            Assert.Equal(2, i[0].Bytes[0]);

            // Test indexer
            i[0] = new InstructionMemory(new int[] { 3 });
            Assert.Equal(3, i[0].Bytes[0]);
        }

        [Fact]
        public void InstructionSet_OneItem_Set1_ThrowsException()
        {
            InstructionSet i = new InstructionSet();
            i.Add(new InstructionMemory(new int[] { 5 }));
            Assert.Throws<AddressOutOfBoundsException>(() => i.Set(1, new int[] { 1 }));
        }

        #endregion

        #region Add

        [Fact]
        public void InstructionSet_Add_ValidAddress0()
        {
            InstructionSet i = new InstructionSet();
            i.Add(new int[] { 5 });
            Assert.Equal(5, i.Get(0).Bytes[0]);
        }

        [Fact]
        public void InstructionSet_Add_InvalidAddressMax()
        {
            InstructionSet iss = new InstructionSet();
            for (int i = 0; i < InstructionSet.InstructionWordSpace; i++)
            {
                iss.Add(new InstructionMemory());
            }

            Assert.Throws<RegisterLimitException>(() => iss.Add(new InstructionMemory()));
        }

        #endregion

    }
}
