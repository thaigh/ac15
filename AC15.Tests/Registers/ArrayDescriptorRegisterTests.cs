﻿using System;
using Xunit;
using AC15.Registers;
using AC15.Memory;
using AC15.Exceptions;

namespace AC15.Tests.Registers
{
    public class ArrayDescriptorRegisterTests
    {
        #region AddOperation

        [Fact]
        public void Add_IncreasesSize_GetAtIndex0()
        {
            ArrayDescriptorRegister reg = new ArrayDescriptorRegister();
            reg.Add(new ArrayDescriptor(1, 2));
            Assert.Equal(1, reg.Size);
            Assert.Equal(1, reg.Get(0).ArraySize);
            Assert.Equal(2, reg.Get(0).ArrayStartAddress);
        }

        [Fact]
        public void Add_ExceedsLimit_ThrowsException()
        {
            ArrayDescriptorRegister reg = new ArrayDescriptorRegister();
            for (int i = 0; i < ArrayDescriptorRegister.MaxArrayDescriptors; i++)
            {
                reg.Add(new ArrayDescriptor(1,1));
            }

            Assert.Equal(ArrayDescriptorRegister.MaxArrayDescriptors, reg.Size);
            Assert.Throws<RegisterLimitException>(() => reg.Add(new ArrayDescriptor(1,1)));
        }

        #endregion

        #region GetOperation

        [Fact]
        public void Get_NoItems_N1_ThrowsEmptyRegisterException()
        {
            ArrayDescriptorRegister reg = new ArrayDescriptorRegister();
            Assert.Equal(0, reg.Size);
            Assert.Throws<EmptyRegisterException>(() => reg.Get(-1));
        }

        [Fact]
        public void Get_NoItems_0_ThrowsEmptyRegisterException()
        {
            ArrayDescriptorRegister reg = new ArrayDescriptorRegister();
            Assert.Equal(0, reg.Size);
            Assert.Throws<EmptyRegisterException>(() => reg.Get(0));
        }

        [Fact]
        public void Get_OneItem_N1_ThrowsException()
        {
            ArrayDescriptorRegister reg = new ArrayDescriptorRegister();
            reg.Add(new ArrayDescriptor(1, 1));
            Assert.Equal(1, reg.Size);
            Assert.Throws<AddressOutOfBoundsException>(() => reg.Get(-1));
        }

        [Fact]
        public void Get_OneItem_0_ReturnsItem()
        {
            ArrayDescriptorRegister reg = new ArrayDescriptorRegister();
            reg.Add(new ArrayDescriptor(1, 2));
            Assert.Equal(1, reg.Size);
            Assert.Equal(1, reg.Get(0).ArraySize);
            Assert.Equal(2, reg.Get(0).ArrayStartAddress);

            Assert.Equal(1, reg[0].ArraySize);
            Assert.Equal(2, reg[0].ArrayStartAddress);
        }

        [Fact]
        public void Get_OneItem_1_ThrowsException()
        {
            ArrayDescriptorRegister reg = new ArrayDescriptorRegister();
            reg.Add(new ArrayDescriptor(1, 1));
            Assert.Equal(1, reg.Size);
            Assert.Throws<AddressOutOfBoundsException>(() => reg.Get(1));
        }

        #endregion

        #region SetOperation

        [Fact]
        public void Set_NoItems_N1_ThrowsException()
        {
            ArrayDescriptorRegister reg = new ArrayDescriptorRegister();
            Assert.Equal(0, reg.Size);
            Assert.Throws<EmptyRegisterException>(() => reg.Set(-1, new ArrayDescriptor(1,1)));
        }

        [Fact]
        public void Set_NoItems_0_ThrowsException()
        {
            ArrayDescriptorRegister reg = new ArrayDescriptorRegister();
            Assert.Equal(0, reg.Size);
            Assert.Throws<EmptyRegisterException>(() => reg.Set(0, new ArrayDescriptor(1, 1)));
        }

        [Fact]
        public void Set_OneItem_N1_ThrowsException()
        {
            ArrayDescriptorRegister reg = new ArrayDescriptorRegister();
            reg.Add(new ArrayDescriptor(1, 1));
            Assert.Equal(1, reg.Size);
            Assert.Throws<AddressOutOfBoundsException>(() => reg.Set(-1, new ArrayDescriptor(1, 1)));
        }

        [Fact]
        public void Set_OneItem_0_SetsNewValue()
        {
            ArrayDescriptorRegister reg = new ArrayDescriptorRegister();
            reg.Add(new ArrayDescriptor(1, 2));
            Assert.Equal(1, reg.Size);
            reg.Set(0, new ArrayDescriptor(3, 4));

            Assert.Equal(3, reg.Get(0).ArraySize);
            Assert.Equal(4, reg.Get(0).ArrayStartAddress);

            reg[0] = new ArrayDescriptor(5, 6);
            Assert.Equal(5, reg[0].ArraySize);
            Assert.Equal(6, reg[0].ArrayStartAddress);
        }

        [Fact]
        public void Set_OneItem_1_ThrowsException()
        {
            ArrayDescriptorRegister reg = new ArrayDescriptorRegister();
            reg.Add(new ArrayDescriptor(1, 1));
            Assert.Equal(1, reg.Size);
            Assert.Throws<AddressOutOfBoundsException>(() => reg.Set(1, new ArrayDescriptor(1, 1)));
        }

        #endregion

        #region ArrayIndexRegister

        [Fact]
        public void ArrayIndexRegister_ArrayIndex()
        {
            ArrayIndexRegister reg = new ArrayIndexRegister();
            reg.ArrayIndex = 1;
            Assert.Equal(1, reg.ArrayIndex);
        }

        #endregion
    }
}
