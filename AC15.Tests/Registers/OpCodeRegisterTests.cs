﻿using System;
using Xunit;
using AC15.Registers;
using AC15.Exceptions;
namespace AC15.Tests.Registers
{
    public class OpCodeRegisterTests
    {

        #region OperandFromBytes

        [Fact]
        public void OpCodeRegister_OperandFromBytes_ByteSingle0_Operand0()
        {
            int[] bytes = { 0 };
            Assert.Equal(0, OpCodeRegister.OperandFromBytes(bytes));
        }

        [Fact]
        public void OpCodeRegister_OperandFromBytes_Byte0_Operand0()
        {
            int[] bytes = { 0, 0, 0, 0 };
            Assert.Equal(0, OpCodeRegister.OperandFromBytes(bytes));
        }

        [Fact]
        public void OpCodeRegister_OperandFromBytes_Byte0001_Operand1()
        {
            int[] bytes = { 0, 0, 0, 1 };
            Assert.Equal(1, OpCodeRegister.OperandFromBytes(bytes));
        }

        [Fact]
        public void OpCodeRegister_OperandFromBytes_Byte000_255_Operand255()
        {
            int[] bytes = { 0, 0, 0, 255 };
            Assert.Equal(255, OpCodeRegister.OperandFromBytes(bytes));
        }

        [Fact]
        public void OpCodeRegister_OperandFromBytes_Byte0010_Operand255()
        {
            int[] bytes = { 0, 0, 1, 0 };
            Assert.Equal(256, OpCodeRegister.OperandFromBytes(bytes));
        }

        [Fact]
        public void OpCodeRegister_OperandFromBytes_Byte0011_Operand255()
        {
            int[] bytes = { 0, 0, 1, 1 };
            Assert.Equal(257, OpCodeRegister.OperandFromBytes(bytes));
        }

        [Fact]
        public void OpCodeRegister_OperandFromBytes_Byte1111_Operand16843009()
        {
            int[] bytes = { 1, 1, 1, 1 };
            Assert.Equal(16843009, OpCodeRegister.OperandFromBytes(bytes));
        }

        [Fact]
        public void OpCodeRegister_OperandFromBytes_Byte000_256_ThrowException()
        {
            int[] bytes = { 0, 0, 0, 256 };
            Assert.Throws<OperandException>(() => OpCodeRegister.OperandFromBytes(bytes));
        }

        [Fact]
        public void OpCodeRegister_OperandFromBytes_Byte00_256_255_ThrowException()
        {
            int[] bytes = { 0, 0, 256, 255 };
            Assert.Throws<OperandException>(() => OpCodeRegister.OperandFromBytes(bytes));
        }

        [Fact]
        public void OpCodeRegister_OperandFromBytes_Byte00_N1_255_ThrowException()
        {
            int[] bytes = { 0, 0, -1, 255 };
            Assert.Throws<OperandException>(() => OpCodeRegister.OperandFromBytes(bytes));
        }

        [Fact]
        public void OpCodeRegister_OperandFromBytes_ByteN1_ThrowException()
        {
            int[] bytes = { -1 };
            Assert.Throws<OperandException>(() => OpCodeRegister.OperandFromBytes(bytes));
        }

        #endregion

        #region BytesFromOperand

        [Fact]
        public void OpCodeRegister_BytesFromOperand_Operand0_Bytes0()
        {
            int[] bytes = { 0 };
            Assert.Equal(bytes, OpCodeRegister.BytesFromOperand(0));
        }

        [Fact]
        public void OpCodeRegister_BytesFromOperand_Operand1_Bytes1() {
            int[] bytes = { 1 };
            Assert.Equal(bytes, OpCodeRegister.BytesFromOperand(1));
        }

        [Fact]
        public void OpCodeRegister_BytesFromOperand_Operand255_Bytes255()
        {
            int[] bytes = { 255 };
            Assert.Equal(bytes, OpCodeRegister.BytesFromOperand(255));
        }

        [Fact]
        public void OpCodeRegister_BytesFromOperand_Operand256_Bytes10()
        {
            int[] bytes = { 1, 0 };
            Assert.Equal(bytes, OpCodeRegister.BytesFromOperand(256));
        }

        [Fact]
        public void OpCodeRegister_BytesFromOperand_Operand257_Bytes11()
        {
            int[] bytes = { 1, 1 };
            Assert.Equal(bytes, OpCodeRegister.BytesFromOperand(257));
        }

        [Fact]
        public void OpCodeRegister_BytesFromOperand_Operand16843009_Bytes1111()
        {
            int[] bytes = { 1, 1, 1, 1 };
            Assert.Equal(bytes, OpCodeRegister.BytesFromOperand(16843009));
        }

        [Fact]
        public void OpCodeRegister_BytesFromOperand_OperandN1_ThrowsException()
        {
            Assert.Throws<OperandException>(() => OpCodeRegister.BytesFromOperand(-1));
        }

        #endregion


        [Fact]
        public void OpCodeRegister_Constructor()
        {
            OpCodeRegister reg = new OpCodeRegister();
            Assert.Equal(-1, reg.OpCode);
            Assert.Equal(-1, reg.Operand);
            Assert.True(reg.InvalidState);
            Assert.Throws<InvalidOperationException>(() => reg.SingleInstruction);
            Assert.Throws<InvalidOperationException>(() => reg.DescriptorOpCode);
            Assert.Equal("[OpCodeRegister: OpCode=-1, Operand=-1]", reg.ToString());
        }

        [Fact]
        public void OpCodeRegister_SetState_Add1()
        {
            OpCodeRegister reg = new OpCodeRegister();
            reg.OpCode = 50; reg.Operand = 1;

            Assert.Equal(50, reg.OpCode);
            Assert.Equal(1, reg.Operand);
            Assert.False(reg.InvalidState);
            Assert.False(reg.SingleInstruction);
            Assert.False(reg.DescriptorOpCode);
            Assert.Equal("[OpCodeRegister: OpCode=50, Operand=1]", reg.ToString());
        }

        [Fact]
        public void OpCodeRegister_SetState_AddArray1()
        {
            OpCodeRegister reg = new OpCodeRegister();
            reg.OpCode = 51; reg.Operand = 1;

            Assert.Equal(51, reg.OpCode);
            Assert.Equal(1, reg.Operand);
            Assert.False(reg.InvalidState);
            Assert.False(reg.SingleInstruction);
            Assert.True(reg.DescriptorOpCode);
            Assert.Equal("[OpCodeRegister: OpCode=51, Operand=1]", reg.ToString());
        }

        [Fact]
        public void OpCodeRegister_SetState_Halt()
        {
            OpCodeRegister reg = new OpCodeRegister();
            reg.OpCode = 1; reg.Operand = -1;

            Assert.Equal(1, reg.OpCode);
            Assert.Equal(-1, reg.Operand);
            Assert.False(reg.InvalidState);
            Assert.True(reg.SingleInstruction);
            Assert.False(reg.DescriptorOpCode);
            Assert.Equal("[OpCodeRegister: OpCode=1, Operand=-1]", reg.ToString());
        }

    }
}
