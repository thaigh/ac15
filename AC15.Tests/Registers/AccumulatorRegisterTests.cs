﻿using AC15.Registers;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Registers
{
    public class AccumulatorRegisterTests
    {

        [Fact]
        public void AccumulatorRegister_FloatingPointValue()
        {
            AccumulatorRegister reg = new AccumulatorRegister();
            reg.Value = 10;
            Assert.Equal("[AccumulatorRegister: CurrentMemoryType=FloatingPoint, Value=10]", reg.ToString());
        }

        [Fact]
        public void AccumulatorRegister_BooleanValue()
        {
            AccumulatorRegister reg = new AccumulatorRegister();
            reg.Value = false;
            Assert.Equal("[AccumulatorRegister: CurrentMemoryType=Boolean, Value=False]", reg.ToString());
        }
    }
}
