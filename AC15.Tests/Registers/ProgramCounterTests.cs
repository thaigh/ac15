﻿using System;
using Xunit;
using AC15.Registers;
using AC15.Exceptions;
namespace AC15.Tests.Registers
{
    public class ProgramCounterTests
    {
        
        [Fact]
        public void PC_Initialise_Default() {
            ProgramCounter pc = new ProgramCounter();
            Assert.Equal(0, pc.Word);
            Assert.Equal(0, pc.Byte);
        }

        [Fact]
        public void PC_Initialise_OverrideSet()
        {
            ProgramCounter pc = new ProgramCounter(1, 2);
            Assert.Equal(1, pc.Word);
            Assert.Equal(2, pc.Byte);
        }

        [Fact]
        public void PC_Initialise_FullAddress7_Word0_Byte7()
        {
            ProgramCounter pc = new ProgramCounter(7);
            Assert.Equal(0, pc.Word);
            Assert.Equal(7, pc.Byte);
        }

        [Fact]
        public void PC_Initialise_FullAddress8_Word1_Byte0()
        {
            ProgramCounter pc = new ProgramCounter(8);
            Assert.Equal(1, pc.Word);
            Assert.Equal(0, pc.Byte);
        }

        [Fact]
        public void PC_Initialise_FullAddress9_Word1_Byte1()
        {
            ProgramCounter pc = new ProgramCounter(9);
            Assert.Equal(1, pc.Word);
            Assert.Equal(1, pc.Byte);
        }

        [Fact]
        public void PC_Increment_FullAddress14_Word1_Byte7()
        {
            ProgramCounter pc = new ProgramCounter(14);
            Assert.Equal(1, pc.Word);
            Assert.Equal(6, pc.Byte);

            pc.Increment();
            Assert.Equal(1, pc.Word);
            Assert.Equal(7, pc.Byte);
        }

        [Fact]
        public void PC_Increment_FullAddress15_Word2_Byte0()
        {
            ProgramCounter pc = new ProgramCounter(15);
            Assert.Equal(1, pc.Word);
            Assert.Equal(7, pc.Byte);

            pc.Increment();
            Assert.Equal(2, pc.Word);
            Assert.Equal(0, pc.Byte);
        }

        [Fact]
        public void PC_IncrementX2_FullAddress15_Word2_Byte1()
        {
            ProgramCounter pc = new ProgramCounter(15);
            Assert.Equal(1, pc.Word);
            Assert.Equal(7, pc.Byte);

            pc.Increment();
            pc.Increment();

            Assert.Equal(2, pc.Word);
            Assert.Equal(1, pc.Byte);
        }

        [Fact]
        public void PC_Jump0_FullAddress17_Word2_Byte1()
        {
            ProgramCounter pc = new ProgramCounter(17);
            Assert.Equal(2, pc.Word);
            Assert.Equal(1, pc.Byte);

            pc.Jump(0);

            Assert.Equal(2, pc.Word);
            Assert.Equal(1, pc.Byte);
        }

        [Fact]
        public void PC_Jump1_FullAddress17_Word2_Byte2()
        {
            ProgramCounter pc = new ProgramCounter(17);
            Assert.Equal(2, pc.Word);
            Assert.Equal(1, pc.Byte);

            pc.Jump(1);

            Assert.Equal(2, pc.Word);
            Assert.Equal(2, pc.Byte);
        }

        [Fact]
        public void PC_Jump7_FullAddress17_Word3_Byte0()
        {
            ProgramCounter pc = new ProgramCounter(17);
            Assert.Equal(2, pc.Word);
            Assert.Equal(1, pc.Byte);

            pc.Jump(7);

            Assert.Equal(3, pc.Word);
            Assert.Equal(0, pc.Byte);
        }

        [Fact]
        public void PC_Jump8_FullAddress17_Word3_Byte1()
        {
            ProgramCounter pc = new ProgramCounter(17);
            Assert.Equal(2, pc.Word);
            Assert.Equal(1, pc.Byte);

            pc.Jump(8);

            Assert.Equal(3, pc.Word);
            Assert.Equal(1, pc.Byte);
        }

        [Fact]
        public void PC_Jump9_FullAddress17_Word3_Byte2()
        {
            ProgramCounter pc = new ProgramCounter(17);
            Assert.Equal(2, pc.Word);
            Assert.Equal(1, pc.Byte);

            pc.Jump(9);

            Assert.Equal(3, pc.Word);
            Assert.Equal(2, pc.Byte);
        }

        [Fact]
        public void PC_JumpN1_FullAddress17_Word2_Byte0()
        {
            ProgramCounter pc = new ProgramCounter(17);
            Assert.Equal(2, pc.Word);
            Assert.Equal(1, pc.Byte);

            pc.Jump(-1);

            Assert.Equal(2, pc.Word);
            Assert.Equal(0, pc.Byte);
        }

        [Fact]
        public void PC_JumpN2_FullAddress17_Word1_Byte7()
        {
            ProgramCounter pc = new ProgramCounter(17);
            Assert.Equal(2, pc.Word);
            Assert.Equal(1, pc.Byte);

            pc.Jump(-2);

            Assert.Equal(1, pc.Word);
            Assert.Equal(7, pc.Byte);
        }

        [Fact]
        public void PC_JumpN8_FullAddress17_Word3_Byte1()
        {
            ProgramCounter pc = new ProgramCounter(17);
            Assert.Equal(2, pc.Word);
            Assert.Equal(1, pc.Byte);

            pc.Jump(-8);

            Assert.Equal(1, pc.Word);
            Assert.Equal(1, pc.Byte);
        }

        [Fact]
        public void PC_JumpN1_FullAddress1_Word0_Byte0()
        {
            ProgramCounter pc = new ProgramCounter(1);
            Assert.Equal(0, pc.Word);
            Assert.Equal(1, pc.Byte);

            pc.Jump(-1);

            Assert.Equal(0, pc.Word);
            Assert.Equal(0, pc.Byte);
        }

        [Fact]
        public void PC_JumpN1_FullAddress0_ThrowsError()
        {
            ProgramCounter pc = new ProgramCounter(0);
            Assert.Equal(0, pc.Word);
            Assert.Equal(0, pc.Byte);

            bool threwException = false;
            try
            {
                pc.Jump(-1);
                threwException = false;
            } catch (InvalidAddressJumpException e) {
                threwException = true;
                Assert.Equal("Cannot jump below 0", e.Message);
            }

            Assert.True(threwException);
        }


        [Fact]
        public void PC_ToString()
        {
            ProgramCounter pc = new ProgramCounter(17);
            Assert.Equal(2, pc.Word);
            Assert.Equal(1, pc.Byte);

            Assert.Equal("[ProgramCounter: Word=2, Byte=1]", pc.ToString());
        }

    }
}
