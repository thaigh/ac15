﻿using System;
using Xunit;
using AC15.Registers;
using AC15.Exceptions;
using AC15.Memory;

namespace AC15.Tests.Registers
{
    public class RamTests
    {
        [Fact]
        public void Ram_Add_ValidAddress0() {
            Ram r = new Ram();
            r.AddFloatingPoint(1);
            Assert.Equal(1, r.GetFloatingPoint(0).Value);
        }

        [Fact]
        public void Ram_EmptyRegister_SetN1_ThrowsEmptyRegisterException()
        {
            Ram r = new Ram();
            Assert.Throws<EmptyRegisterException>( () => r.SetFloatingPoint(-1, 1));
        }

        [Fact]
        public void Ram_EmptyRegister_GetN1_ThrowsEmptyRegisterException()
        {
            Ram r = new Ram();
            Assert.Throws<EmptyRegisterException>(() => r.GetFloatingPoint(-1));
        }

        [Fact]
        public void Ram_EmptyRegister_Get0_ThrowsEmptyRegisterException()
        {
            Ram r = new Ram();
            Assert.Throws<EmptyRegisterException>(() => r.GetFloatingPoint(0));
        }

        [Fact]
        public void Ram_OneItem_GetN1_ThrowsAddressOutOfBoundsException()
        {
            Ram r = new Ram();
            r.AddFloatingPoint(1);
            Assert.Throws<AddressOutOfBoundsException>(() => r.Get(-1));
        }

        [Fact]
        public void Ram_OneItem_Get0_Returns1()
        {
            Ram r = new Ram();
            r.AddFloatingPoint(1);
            IMemory mem = r.Get(0);

            Assert.IsType<FloatingPointMemory>(mem);
            FloatingPointMemory fp = (FloatingPointMemory)mem;

            Assert.Equal(1, fp.Value);
        }

        [Fact]
        public void Ram_OneItem_Get1_ThrowsAddressOutOfBoundsException()
        {
            Ram r = new Ram();
            r.AddFloatingPoint(1);
            Assert.Throws<AddressOutOfBoundsException>(() => r.Get(1));
        }

        [Fact]
        public void Ram_Add_InvalidAddressMax_ThrowsRegisterLimitException()
        {
            Ram r = new Ram();

            for (int i = 0; i < Ram.MemoryWordSpace; i++)
            {
                r.AddFloatingPoint(0);
            }

            Assert.Throws<RegisterLimitException>(() => r.AddFloatingPoint(1));
        }

    }
}
