﻿using AC15.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Exceptions
{
    public class InvalidStringExceptionTests
    {

        [Fact]
        public void InvalidStringException_Constructor()
        {
            InvalidStringException exc = new InvalidStringException();
            Assert.Equal("Invalid format for String Memory", exc.Message);
        }

        [Fact]
        public void InvalidStringException_MessageConstructor()
        {
            InvalidStringException exc = new InvalidStringException("Custom Message");
            Assert.Equal("Custom Message", exc.Message);
        }
    }
}
