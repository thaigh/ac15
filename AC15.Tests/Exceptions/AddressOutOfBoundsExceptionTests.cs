﻿using AC15.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Exceptions
{
    public class AddressOutOfBoundsExceptionTests
    {

        [Fact]
        public void AddressOutOfBounds_Constructor()
        {
            AddressOutOfBoundsException exc = new AddressOutOfBoundsException(1, 2, 3);
            Assert.Equal(1, exc.LowerBound);
            Assert.Equal(2, exc.UpperBound);
            Assert.Equal(3, exc.AttemptedAddress);
            Assert.Equal("Attempted to access Address 3 that is out of bounds [1, 2]", exc.Message);
        }

        [Fact]
        public void AddressOutOfBounds_ConstructorWithMessage()
        {
            AddressOutOfBoundsException exc = new AddressOutOfBoundsException(1, 2, 3, "Custom Message");
            Assert.Equal(1, exc.LowerBound);
            Assert.Equal(2, exc.UpperBound);
            Assert.Equal(3, exc.AttemptedAddress);
            Assert.Equal("Custom Message", exc.Message);
        }

        [Theory]
        [InlineData(1,3,1)]
        [InlineData(1,3,2)]
        [InlineData(1,3,3)]
        [InlineData(1,1,1)]
        public void AddressBoundary_ValidAddress(int lower, int upper, int attempt)
        {
            // Expect that this does not throw an exception
            AddressBoundary.ValidateAddress(attempt, lower, upper, "TestBoundary");
        }

        [Theory]
        [InlineData(1,3,0)]
        [InlineData(1,3,4)]
        public void AddressBoundary_InvalidAddress_ThrowException(int lower, int upper, int attempt)
        {
            Assert.Throws<AddressOutOfBoundsException>(() => AddressBoundary.ValidateAddress(attempt, lower, upper, "TestBoundary"));
        }

        [Fact]
        public void AddressBoundary_InvalidBounds_ThrowException()
        {
            Assert.Throws<ArgumentException>(() => AddressBoundary.ValidateAddress(1, 1, 0, "TestBoundary"));

        }
    }
}
