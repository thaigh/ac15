﻿using AC15.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Exceptions
{
    public class InvalidAddressJumpExceptionTests
    {

        [Fact]
        public void InvalidAddressJumpException_Constructor()
        {
            InvalidAddressJumpException exc = new InvalidAddressJumpException();
            Assert.Equal("Cannot jump to address", exc.Message);
        }

        [Fact]
        public void InvalidAddressJumpException_MessageConstructor()
        {
            InvalidAddressJumpException exc = new InvalidAddressJumpException("Custom Message");
            Assert.Equal("Custom Message", exc.Message);
        }
    }
}
