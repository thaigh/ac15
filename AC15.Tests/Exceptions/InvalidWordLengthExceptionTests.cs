﻿using AC15.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Exceptions
{
    public class InvalidWordLengthExceptionTests
    {

        [Fact]
        public void InvalidWordLengthException_Constructor()
        {
            InvalidWordLengthException exc = new InvalidWordLengthException();
            Assert.Equal("Invalid Word Length", exc.Message);
        }

        [Fact]
        public void InvalidWordLengthException_MessageConstructor()
        {
            InvalidWordLengthException exc = new InvalidWordLengthException("Custom Message");
            Assert.Equal("Custom Message", exc.Message);
        }
    }
}
