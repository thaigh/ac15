﻿using AC15.Exceptions;
using AC15.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Exceptions
{
    public class InvalidMemoryCastExceptionTests
    {

        [Fact]
        public void InvalidMemoryCastException_Constructor()
        {
            InvalidMemoryCastException exc = new InvalidMemoryCastException(MemoryType.FloatingPoint, MemoryType.ArrayDescriptor);
            Assert.Equal(MemoryType.FloatingPoint, exc.Expected);
            Assert.Equal(MemoryType.ArrayDescriptor, exc.Actual);
            Assert.Equal("Invalid Memory Cast. Expected FloatingPoint but got ArrayDescriptor", exc.Message);
        }

        [Fact]
        public void InvalidMemoryCastException_MessageConstructor()
        {
            InvalidMemoryCastException exc = new InvalidMemoryCastException(MemoryType.FloatingPoint, MemoryType.ArrayDescriptor, "Custom Message");
            Assert.Equal(MemoryType.FloatingPoint, exc.Expected);
            Assert.Equal(MemoryType.ArrayDescriptor, exc.Actual);
            Assert.Equal("Custom Message", exc.Message);
        }
    }
}
