﻿using AC15.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Exceptions
{
    public class RegisterLimitExceptionTests
    {

        [Fact]
        public void RegisterLimitException_Constructor()
        {
            RegisterLimitException exc = new RegisterLimitException();
            Assert.Equal("Maximum number of items in this register have been registered", exc.Message);
        }

        [Fact]
        public void RegisterLimitException_MessageConstructor()
        {
            RegisterLimitException exc = new RegisterLimitException("Custom Message");
            Assert.Equal("Custom Message", exc.Message);
        }

    }
}
