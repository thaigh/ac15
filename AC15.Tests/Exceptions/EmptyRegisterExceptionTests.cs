﻿using AC15.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Exceptions
{
    public class EmptyRegisterExceptionTests
    {

        [Fact]
        public void EmptyRegisterException_Constructor()
        {
            EmptyRegisterException exc = new EmptyRegisterException(1);
            Assert.Equal(1, exc.Address);
            Assert.Equal("Cannot access address of empty register", exc.Message);
        }

        [Fact]
        public void EmptyRegisterException_ConstructorWithMessage()
        {
            EmptyRegisterException exc = new EmptyRegisterException(2, "Custom Message");
            Assert.Equal(2, exc.Address);
            Assert.Equal("Custom Message", exc.Message);
        }

    }
}
