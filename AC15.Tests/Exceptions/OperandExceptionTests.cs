﻿using AC15.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Exceptions
{
    public class OperandExceptionTests
    {

        [Fact]
        public void OperandException_Constructor()
        {
            OperandException exc = new OperandException();
            Assert.Equal("Invalid operand", exc.Message);
        }

        [Fact]
        public void OperandException_MessageConstructor()
        {
            OperandException exc = new OperandException("Custom Message");
            Assert.Equal("Custom Message", exc.Message);
        }

    }
}
