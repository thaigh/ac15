﻿using AC15.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AC15.Tests.Exceptions
{
    public class CodeModuleExceptionTests
    {
        [Fact]
        public void CodeModuleException_Constructor()
        {
            CodeModuleException exc = new CodeModuleException();
            Assert.Equal("Error ocurred when loading code module", exc.Message);
        }

        [Fact]
        public void CodeModuleException_StringMessageConstructor()
        {
            CodeModuleException exc = new CodeModuleException("Custom Message");
            Assert.Equal("Custom Message", exc.Message);
        }
    }
}
