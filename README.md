AC15
====

A C# implementation of the Accumulator Machine architecture used as the target machine for
[Compiler Design (COMP3290)](https://www.newcastle.edu.au/course/COMP3290) studied at the University of Newcastle
2015 as developed by Mike Hannaford and inspired by by Digico Microsystem's M16V architecture.

The AC15 Accumulator Architecture means that most of the machine instructions include the operand information to
be operated on a single register (the Accumulator's primary register)

The AC15 architecture supports the following data types:

* Floating Point numbers
* Boolean values
* Arrays

The architecture ensures proper data type protection to ensure that operations are used in a way that is consistent
with how the data type should be used.

    For Example: The ADD instruction can only be used with numeric data types
    For Example: The AND instruction can only be used with boolean data types

# Usage #

    TODO: Add instructions for using the AC15.exe
    Use C# Command Line Args library to enforce usage

# Op Codes Instruction Set #

The instruction set for the AC15 machine is broken into three sections:

* Single Byte Instructions
* Auto-Indirect Instructions
* Other

## Single Byte Instructions ##

|Op Code|Instruction|Description|
|-------|-----------|-----------|
|0|Halt|Stop program execution. Close the program|
|1|No Operation|Does nothing. Used to pad instructions in the code file|
|2|Clear|Clears the accumulator's primary register. Set ACC=0.0|
|3|Change Sign|Negates the sign of the Accumulator. Only works when accumulator is set to a numeric value|
|4|Absolute Value|Sets the Accumulator to the Absolute Value of the primary register. Only works with numeric values|

