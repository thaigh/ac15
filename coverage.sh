rm -rf coverage

mkdir -p coverage
mkdir -p coverage/report

coverlet ./AC15.Tests/bin/Debug/netcoreapp2.0/AC15.Tests.dll --target "dotnet" --targetargs "test ./AC15.Tests --no-build" --output "./coverage/" --format opencover


dotnet ~/Documents/Apps/ReportGenerator_4.0.4.0/netcoreapp2.0/ReportGenerator.dll "-reports:coverage/coverage.opencover.xml" "-targetdir:coverage/report"