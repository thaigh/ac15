﻿using System;
using AC15.File;
using AC15.Registers;
using AC15.Cpu;

namespace AC15
{
    class Program
    {
        static void Main(string[] args)
        {
            AccumulatorCpu cpu = new AccumulatorCpu();
            cpu.LoadProgramFromFile("../SampleFiles/simple.ac15");
            cpu.Run();

        }
    }
}
