﻿using System;
using System.ComponentModel;

namespace AC15.Memory
{
    public enum MemoryType {
        FloatingPoint,
        Instruction,
        ArrayDescriptor,
        SubprogramDescriptor,
        String,
        MemoryAddress,
        Boolean
    }

    public interface IMemory {
        MemoryType MemoryType { get; }
    }

    public class FloatingPointMemory : IMemory {
        public const int DefaultValue = 0;

        public MemoryType MemoryType => MemoryType.FloatingPoint;
        public readonly double Value;

        public FloatingPointMemory() : this(DefaultValue) { }
        public FloatingPointMemory(double val) { this.Value = val; }

        public override string ToString()
        {
            return string.Format("[FloatingPointMemory: Value={0}]", Value);
        }
    }

    public class BooleanMemory : IMemory {
        public const bool DefaultValue = false;
        
        public MemoryType MemoryType => MemoryType.Boolean;
        public readonly bool Value;

        public BooleanMemory() : this(DefaultValue) { }
        public BooleanMemory(bool val) { this.Value = val; }

        public override string ToString()
        {
            return string.Format("[BooleanMemory: Value={0}]", Value);
        }

    }

    public class ArrayDescriptor : IMemory
    {
        public MemoryType MemoryType => MemoryType.ArrayDescriptor;

        public readonly int ArrayStartAddress;
        public readonly int ArraySize;

        public ArrayDescriptor(int size, int startAddress)
        {
            this.ArraySize = size;
            this.ArrayStartAddress = startAddress;
        }

        public override string ToString()
        {
            return string.Format("[ArrayDescriptor: ArrayStartAddress={0}, ArraySize={1}]", ArrayStartAddress, ArraySize);
        }
    }

    public class SubprogramDescriptor : IMemory
    {
        public MemoryType MemoryType => MemoryType.SubprogramDescriptor;

        public readonly int EntryAddress;
        public int ReturnAddress { get; set; }

        public SubprogramDescriptor(int entryAddress, int returnAddress)
        {
            this.EntryAddress = entryAddress;
            this.ReturnAddress = returnAddress;
        }

        public override string ToString()
        {
            return string.Format("[SubprogramDescriptor: EntryAddress={0}, ReturnAddress={1}]", EntryAddress, ReturnAddress);
        }
    }

    public class MemoryAddress : IMemory {
        public MemoryType MemoryType => MemoryType.MemoryAddress;
        public readonly int AddressIndex;

        public MemoryAddress(int addressIndex) { this.AddressIndex = addressIndex; }

        public override string ToString()
        {
            return string.Format("[MemoryAddress: AddressIndex={0}]", AddressIndex);
        }
    }

}
