﻿using AC15.Exceptions;
using System.Text;
using System;
using System.Linq;

namespace AC15.Memory
{
    /// <summary>
    /// Contains an 8 byte length sequence of character information
    /// that can be printed to the output stream
    /// </summary>
    public class StringMemory : IMemory
    {
        public MemoryType MemoryType => MemoryType.String;

        public const int WordLength = 8;
        private static readonly char[] NullStringBytes = Enumerable.Repeat((char)0, WordLength).ToArray();
        public static readonly StringMemory NullString = new StringMemory(NullStringBytes);

        public string CharacterString { get { return GetCharacterString(); } set { SetCharacters(value); } }
        public char[] Characters { get { return GetCharacters(); } set { SetCharacters(value); } }
        private char[] _characters = new char[WordLength];


        public StringMemory() { ClearStringData();  }
        public StringMemory(char[] chars) { SetCharacters(chars); }
        public StringMemory(string str) { SetCharacters(str); }


        public char[] GetCharacters()
        {
            char[] copy = new char[WordLength];
            for (int i = 0; i < WordLength; i++) { copy[i] = _characters[i]; }
            return copy;
        }

        public string GetCharacterString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var c in Characters) { sb.Append((c == (char)0) ? "" : "" + c); }
            return sb.ToString();
        }

        public void SetCharacters(string str) { SetCharacters(str.ToCharArray()); }

        public void SetCharacters(char[] chars)
        {
            PreconditionCharacterArrayLength(chars.Length);

            char[] newChars = new char[WordLength];
            for (int i = 0; i < WordLength; i++) { newChars[i] = (i >= chars.Length) ? (char)0 : chars[i]; }

            _characters = newChars;
        }

        private void PreconditionCharacterArrayLength(int charLength)
        {
            if (charLength > WordLength)
                throw new InvalidStringException(string.Format("String Memory Length cannot exceed {0} characters", WordLength));
        }

        private void ClearStringData()
        {
            _characters = new char[WordLength];
            for (int i = 0; i < WordLength; i++) { _characters[i] = (char)0; }
        }

        public override string ToString()
        {
            const string Delimiter = ", ";
            string s = string.Join(Delimiter, _characters);

            return string.Format("[String: [{0}]]", s);
        }
    }

}
