﻿using AC15.Exceptions;
using AC15.Instructions;
using System.Linq;

namespace AC15.Memory
{
    public class InstructionMemory : IMemory
    {
        public const int WordLength = 8;
        private static readonly int[] NoOpBytes = Enumerable.Repeat((int)OpCode.NoOp, WordLength).ToArray();
        public static readonly InstructionMemory NoOperation = new InstructionMemory(NoOpBytes);


        public MemoryType MemoryType => MemoryType.Instruction;

        private int[] _bytes = new int[WordLength];
        public int[] Bytes { get { return GetBytes(); } set { SetBytes(value); } }

        public InstructionMemory()
        {
            ClearInstruction();
        }

        public InstructionMemory(int[] byteData)
        {
            SetBytes(byteData);
        }

        public int[] GetBytes() {
            int[] copy = new int[WordLength];
            for (int i = 0; i < WordLength; i++) { copy[i] = _bytes[i]; }
            return copy;
        }

        public void ClearInstruction()
        {
            _bytes = new int[WordLength];
            for (int i = 0; i < WordLength; i++) { _bytes[i] = (int)OpCode.NoOp; }
        }

        public void SetBytes(int[] byteData)
        {
            PreconditionCheckByteDataLength(byteData);

            int[] bytes = new int[WordLength];
            for (int i = 0; i < WordLength; i++) { bytes[i] = (i >= byteData.Length) ? (int)OpCode.NoOp : byteData[i]; }

            _bytes = bytes;
        }

        public override string ToString()
        {
            const string Delimiter = ", ";
            string b = string.Join(Delimiter, _bytes);

            return string.Format("[Instruction: Bytes=[{0}]]", b);
        }

        private void PreconditionCheckByteDataLength(int[] byteData)
        {
            if (byteData.Length > WordLength)
                throw new InvalidWordLengthException(string.Format("Instruction memory only supports {0} bytes of data", WordLength));
        }
    }
}
