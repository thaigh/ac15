﻿using System;
using AC15.Cpu;
using AC15.Memory;
namespace AC15.Instructions
{

    public interface IInstruction {
        OpCode Code { get; }
        IMemory EffectiveOperand { get; set; }
        void Execute(IAccumulator acc);
    }

    public class HaltInstruction : IInstruction
    {
        public OpCode Code => OpCode.Halt;

        public IMemory EffectiveOperand {
            get { throw new InvalidOperationException(); }
            set { throw new InvalidOperationException(); }
        }

        public void Execute(IAccumulator acc)
        {
            // Do Nothing
        }

        public override string ToString()
        {
            return string.Format("[HaltInstruction: Code={0}]", Code);
        }
    }

}
