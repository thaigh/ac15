using System;
using System.Collections.Generic;
using System.Text;
using AC15.Cpu;
using AC15.Memory;

namespace AC15.Instructions
{
    public class AccumulatorToIndexInstruction : IInstruction
    {
        public OpCode Code => OpCode.AccumulatorToIndex;

        public IMemory EffectiveOperand {
            get => throw new InvalidOperationException();
            set => throw new InvalidOperationException();
        }

        public void Execute(IAccumulator acc) { acc.LoadIndexFromAccumulator(); }

        public override string ToString()
        {
            return string.Format("[AccumulatorToIndexInstruction: Code={0}]", Code);
        }
    }

    public class IncrementIndexInstruction : IInstruction
    {
        public OpCode Code => OpCode.IncrementIndex;

        public IMemory EffectiveOperand {
            get => throw new InvalidOperationException();
            set => throw new InvalidOperationException();
        }

        public void Execute(IAccumulator acc) { acc.IncrementIndex(); }

        public override string ToString()
        {
            return string.Format("[IncrementIndexInstruction: Code={0}]", Code);
        }
    }

    public class DecrementIndexInstruction : IInstruction
    {
        public OpCode Code => OpCode.DecrementIndex;

        public IMemory EffectiveOperand {
            get => throw new InvalidOperationException();
            set => throw new InvalidOperationException();
        }

        public void Execute(IAccumulator acc) { acc.DecrementIndex(); }

        public override string ToString()
        {
            return string.Format("[DecrementIndexInstruction: Code={0}]", Code);
        }
    }

    public class ResetIndexInstruction : IInstruction
    {
        public OpCode Code => OpCode.ResetIndex;

        public IMemory EffectiveOperand {
            get => throw new InvalidOperationException();
            set => throw new InvalidOperationException();
        }

        public void Execute(IAccumulator acc) { acc.ResetIndex(); }

        public override string ToString()
        {
            return string.Format("[ResetIndexInstruction: Code={0}]", Code);
        }
    }

    public class LoadIndexInstruction : IInstruction
    {
        public OpCode Code => OpCode.LoadIndex;

        private MemoryAddress _address = new MemoryAddress(0);
        public IMemory EffectiveOperand {
            get => _address;
            set => _address = (MemoryAddress)value;
        }

        public int AddressIndex => _address.AddressIndex;

        public LoadIndexInstruction() : this(0) { }
        public LoadIndexInstruction(int address) { _address = new MemoryAddress(address); }

        public void Execute(IAccumulator acc) { acc.LoadIndexFromAddress(AddressIndex); }

        public override string ToString()
        {
            return string.Format("[LoadIndexInstruction: Code={0}, AddressIndex={1}]", Code, AddressIndex);
        }
    }


}