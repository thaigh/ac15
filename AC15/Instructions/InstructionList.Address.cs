﻿using System;
using AC15.Cpu;
using AC15.Memory;

namespace AC15.Instructions
{
    public class GetInstruction : IInstruction
    {
        public OpCode Code => OpCode.Get;

        private MemoryAddress _addr;
        public IMemory EffectiveOperand { get { return _addr; } set { _addr = (MemoryAddress)value; } }
        public int Value { get { return _addr.AddressIndex; } set { _addr = new MemoryAddress(value); } }

        public GetInstruction() : this(0) { }
        public GetInstruction(int address) { Value = address; }

        public void Execute(IAccumulator acc) { acc.GetMemoryFromAddress(Value); }

        public override string ToString()
        {
            return string.Format("[GetInstruction: Code={0}, EffectiveOperand={1}, Value={2}]", Code, EffectiveOperand, Value);
        }
    }

    public class StoreInstruction : IInstruction
    {
        public OpCode Code => OpCode.Store;

        private MemoryAddress _addr;
        public IMemory EffectiveOperand { get { return _addr; } set { _addr = (MemoryAddress)value; } }
        public int Value { get { return _addr.AddressIndex; } set { _addr = new MemoryAddress(value); } }

        public StoreInstruction() : this(0) { }
        public StoreInstruction(int address) { Value = address; }

        public void Execute(IAccumulator acc) { acc.StoreMemoryToAddress(Value); }

        public override string ToString()
        {
            return string.Format("[StoreInstruction: Code={0}, EffectiveOperand={1}, Value={2}]", Code, EffectiveOperand, Value);
        }
    }
}
