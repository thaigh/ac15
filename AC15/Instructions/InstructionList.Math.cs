﻿using System;
using AC15.Cpu;
using AC15.Memory;

namespace AC15.Instructions
{
    public class AddInstruction : IInstruction
    {
        public OpCode Code => OpCode.Add;

        private FloatingPointMemory _fp = new FloatingPointMemory();
        public IMemory EffectiveOperand { get { return _fp; } set { _fp = (FloatingPointMemory)value; } }
        public double Value { get { return _fp.Value; } set { _fp = new FloatingPointMemory(value); } }

        public AddInstruction() : this(0) { }
        public AddInstruction(double value) { Value = value; }

        public void Execute(IAccumulator acc) { acc.Add(Value); }

        public override string ToString()
        {
            return string.Format("[AddInstruction: Code={0}, EffectiveOperand={1}]", Code, EffectiveOperand);
        }
    }

    public class SubtractInstruction : IInstruction
    {
        public OpCode Code => OpCode.Subtract;

        private FloatingPointMemory _fp = new FloatingPointMemory();
        public IMemory EffectiveOperand { get { return _fp; } set { _fp = (FloatingPointMemory)value; } }
        public double Value { get { return _fp.Value; } set { _fp = new FloatingPointMemory(value); } }

        public SubtractInstruction() : this(0) { }
        public SubtractInstruction(double value) { Value = value; }

        public void Execute(IAccumulator acc) { acc.Subtract(Value); }

        public override string ToString()
        {
            return string.Format("[SubtractInstruction: Code={0}, EffectiveOperand={1}]", Code, EffectiveOperand);
        }
    }

    public class MultiplyInstruction : IInstruction
    {
        public OpCode Code => OpCode.Multiply;

        private FloatingPointMemory _fp = new FloatingPointMemory();
        public IMemory EffectiveOperand { get { return _fp; } set { _fp = (FloatingPointMemory)value; } }
        public double Value { get { return _fp.Value; } set { _fp = new FloatingPointMemory(value); } }

        public MultiplyInstruction() : this(0) { }
        public MultiplyInstruction(double value) { Value = value; }

        public void Execute(IAccumulator acc) { acc.Multiply(Value); }

        public override string ToString()
        {
            return string.Format("[MultiplyInstruction: Code={0}, EffectiveOperand={1}]", Code, EffectiveOperand);
        }
    }

    public class DivideInstruction : IInstruction
    {
        public OpCode Code => OpCode.Divide;

        private FloatingPointMemory _fp = new FloatingPointMemory();
        public IMemory EffectiveOperand { get { return _fp; } set { _fp = (FloatingPointMemory)value; } }
        public double Value { get { return _fp.Value; } set { _fp = new FloatingPointMemory(value); } }

        public DivideInstruction() : this(0) { }
        public DivideInstruction(double value) { Value = value; }

        public void Execute(IAccumulator acc) { acc.Divide(Value); }

        public override string ToString()
        {
            return string.Format("[DivideInstruction: Code={0}, EffectiveOperand={1}]", Code, EffectiveOperand);
        }
    }


    public class GreaterThanInstruction : IInstruction
    {
        // In AC15, the Instruction will always call GREATER THAN 0
        
        public OpCode Code => OpCode.GreaterThan;

        public IMemory EffectiveOperand {
            get => throw new InvalidOperationException();
            set => throw new InvalidOperationException();
        }

        public GreaterThanInstruction() { }

        public void Execute(IAccumulator acc) { acc.GreaterThan(); }

        public override string ToString()
        {
            return string.Format("[GreaterThanInstruction: Code={0}]", Code);
        }
    }


    public class GreaterThanOrEqualInstruction : IInstruction
    {
        // In AC15, the Instruction will always call GREATER THAN OR EQUAL TO 0
        
        public OpCode Code => OpCode.GreaterThanEqual;

        public IMemory EffectiveOperand {
            get => throw new InvalidOperationException();
            set => throw new InvalidOperationException();
        }

        public GreaterThanOrEqualInstruction() { }

        public void Execute(IAccumulator acc) { acc.GreaterThanOrEqual(); }

        public override string ToString()
        {
            return string.Format("[GreaterThanOrEqualInstruction: Code={0}]", Code);
        }
    }

    public class LessThanInstruction : IInstruction
    {
        // In AC15, the Instruction will always call LESS THAN 0
        
        public OpCode Code => OpCode.LessThan;

        public IMemory EffectiveOperand {
            get => throw new InvalidOperationException();
            set => throw new InvalidOperationException();
        }

        public LessThanInstruction() { }

        public void Execute(IAccumulator acc) { acc.LessThan(); }

        public override string ToString()
        {
            return string.Format("[LessThanInstruction: Code={0}]", Code);
        }
    }


    public class LessThanOrEqualInstruction : IInstruction
    {
        // In AC15, the Instruction will always call LESS THAN OR EQUAL TO 0
        
        public OpCode Code => OpCode.LessThanEqual;

        public IMemory EffectiveOperand {
            get => throw new InvalidOperationException();
            set => throw new InvalidOperationException();
        }

        public LessThanOrEqualInstruction() { }

        public void Execute(IAccumulator acc) { acc.LessThanOrEqual(); }

        public override string ToString()
        {
            return string.Format("[LessThanOrEqualInstruction: Code={0}]", Code);
        }
    }


    public class EqualToInstruction : IInstruction
    {
        // In AC15, the Instruction will always call EQUAL TO THAN 0
        
        public OpCode Code => OpCode.EqualTo;

        public IMemory EffectiveOperand {
            get => throw new InvalidOperationException();
            set => throw new InvalidOperationException();
        }

        public EqualToInstruction() { }

        public void Execute(IAccumulator acc) { acc.EqualTo(); }

        public override string ToString()
        {
            return string.Format("[EqualToInstruction: Code={0}]", Code);
        }
    }


    public class NotEqualToInstruction : IInstruction
    {
        // In AC15, the Instruction will always call NOT EQUAL TO 0
        
        public OpCode Code => OpCode.NotEqualTo;

        public IMemory EffectiveOperand {
            get => throw new InvalidOperationException();
            set => throw new InvalidOperationException();
        }

        public NotEqualToInstruction() { }

        public void Execute(IAccumulator acc) { acc.NotEqualTo(); }

        public override string ToString()
        {
            return string.Format("[NotEqualToInstruction: Code={0}]", Code);
        }
    }


}
