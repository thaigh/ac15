﻿using System;
using AC15.Cpu;
using AC15.Memory;

namespace AC15.Instructions
{

    public class ReadFloatInstruction : IInstruction
    {
        public OpCode Code => OpCode.ReadFloat;
        public IMemory EffectiveOperand { get => throw new InvalidOperationException(); set => throw new InvalidOperationException(); }

        public ReadFloatInstruction() { }

        public void Execute(IAccumulator acc) { acc.ReadFloat(); }

        public override string ToString()
        {
            return string.Format("[ReadFloatInstruction Code={0}]", Code);
        }
    }

    public class ReadIntegerInstruction : IInstruction
    {
        public OpCode Code => OpCode.ReadInt;
        public IMemory EffectiveOperand { get => throw new InvalidOperationException(); set => throw new InvalidOperationException(); }

        public ReadIntegerInstruction() { }

        public void Execute(IAccumulator acc) { acc.ReadInteger(); }

        public override string ToString()
        {
            return string.Format("[ReadIntegerInstruction Code={0}]", Code);
        }
    }

    public class ValuePrintInstruction : IInstruction
    {
        public OpCode Code => OpCode.ValuePrint;

        //public IMemory EffectiveOperand { get; set; } // Optional. Not required.
        public IMemory EffectiveOperand
        {
            get { throw new InvalidOperationException(); }
            set { throw new InvalidOperationException(); }
        }

        public void Execute(IAccumulator acc) { acc.PrintAccumulatorValue(); }

        public override string ToString()
        {
            return string.Format("[ValuePrintInstruction]");
        }
    }

    public class StringPrintInstruction : IInstruction
    {
        public OpCode Code => OpCode.StringPrint;
        public IMemory EffectiveOperand { get; set; }

        private StringMemory _stringValue => (StringMemory)EffectiveOperand;

        public string Value
        {
            get { return _stringValue.CharacterString; }
            set { EffectiveOperand = new StringMemory(value); }
        }

        public StringPrintInstruction() : this("") { }
        public StringPrintInstruction(string val) { this.Value = val; }

        public void Execute(IAccumulator acc) { acc.PrintString(Value); }

        public override string ToString()
        {
            return string.Format("[StringPrintInstruction Value={0}]", Value);
        }
    }

    public class NewLineInstruction : IInstruction
    {
        public OpCode Code => OpCode.NewLine;

        public IMemory EffectiveOperand {
            get { throw new InvalidOperationException(); }
            set { throw new InvalidOperationException(); }
        }

        public void Execute(IAccumulator acc) { acc.PrintNewLine(); }

        public override string ToString()
        {
            return string.Format("[NewLineInstruction]");
        }
    }

    public class SpacePrintInstruction : IInstruction
    {
        public OpCode Code => OpCode.Space;

        public IMemory EffectiveOperand
        {
            get { throw new InvalidOperationException(); }
            set { throw new InvalidOperationException(); }
        }

        public void Execute(IAccumulator acc) { acc.PrintSpace(); }

        public override string ToString()
        {
            return string.Format("[SpacePrintInstruction]");
        }
    }

}
