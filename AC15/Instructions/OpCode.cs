﻿using System;
namespace AC15.Instructions
{
    public enum OpCode
    {
        Halt = 0,
        NoOp = 1,
        Clear = 2,
        ChangeSign = 3,
        Abs = 4,
        Negate = 5,

        GreaterThan = 10,
        GreaterThanEqual = 11,
        LessThan = 12,
        LessThanEqual = 13,

        EqualTo = 14,
        NotEqualTo = 15,
        BooleanEqualTo = 16,
        BooleanNotEqualTo = 17,

        ReadFloat = 20,
        ReadInt = 21,
        ValuePrint = 22,
        StringPrint = 23,
        CharacterPrint = 24,
        NewLine = 25,
        Space = 26,

        AccumulatorToIndex = 30,
        IncrementIndex = 31,
        DecrementIndex = 32,
        ResetIndex = 33,

        Add = 50,
        AddDescriptor = 51,
        Subtract = 52,
        SubtractDescriptor = 53,
        Multiply = 54,
        MultiplyDescriptor = 55,
        Divide = 56,
        DivideDescriptor = 57,
        IntDivide = 58,
        IntDivideDescriptor = 59,

        And = 60,
        AndDescriptor = 61,
        Or = 62,
        OrDescriptor = 63,
        Xor = 64,
        XorDescriptor = 65,

        Get = 66,
        GetDescriptor = 67,
        Store = 68,
        StoreDescriptor = 69,
        LoadIndex = 72,

        Call = 81,
        Return = 83,
        GetAddress = 84,
        GetAddressDescriptor = 85,
        GetMemoryDescriptor = 86,
        GetArrayDescriptor = 87,
        StoreDirect = 88,

        ConditionalBranchBack = 90,
        ConditionalBranchForward = 92,
        BranchBack = 94,
        BranchForward = 96
    }

    public enum OpCodeDataType {
        SingleInstruction,
        FloatingPoint,
        Boolean,
        GenericMemory,
        Address,
        Subprogram,
        Other
    }

    public static class OpCodeExtensions {
        public static OpCodeDataType AssociatedDataType(this OpCode op) {
            if (Between(op, OpCode.Halt, OpCode.ResetIndex)) return OpCodeDataType.SingleInstruction;
            if (Between(op, OpCode.Add, OpCode.IntDivideDescriptor)) return OpCodeDataType.FloatingPoint;
            if (Between(op, OpCode.And, OpCode.XorDescriptor)) return OpCodeDataType.Boolean;
            if (Between(op, OpCode.Get, OpCode.GetDescriptor)) return OpCodeDataType.Address;
            if (Between(op, OpCode.Store, OpCode.StoreDescriptor)) return OpCodeDataType.Address;
            if (op == OpCode.LoadIndex) return OpCodeDataType.Address;
            if (Between(op, OpCode.Call, OpCode.Return)) return OpCodeDataType.Subprogram;
            if (Between(op, OpCode.GetAddress, OpCode.StoreDirect)) return OpCodeDataType.Address;
            if (Between(op, OpCode.ConditionalBranchBack, OpCode.BranchForward)) return OpCodeDataType.Other;

            return OpCodeDataType.Other;
        }

        private static bool Between(Enum e, Enum start, Enum end) {
            return Between(e.ToInt(), start.ToInt(), end.ToInt());
        }

        private static int ToInt(this Enum e) {
            return Convert.ToInt32(e);
        }

        private static bool Between(int a, int start, int end)
        {
            return a >= start && a <= end;
        }
    }
}
