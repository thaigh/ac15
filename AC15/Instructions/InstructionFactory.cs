﻿using System;
using System.Collections.Generic;
namespace AC15.Instructions
{
    public interface IFactory<TKey, TVal> {
        TVal Create(TKey key);
    }

    public class InstructionFactory : IFactory<OpCode, IInstruction>
    {
        public IInstruction Create(OpCode key)
        {
            switch(key) {
                // Math Instructions
                case OpCode.Add:
                case OpCode.AddDescriptor: return new AddInstruction();
                case OpCode.Subtract:
                case OpCode.SubtractDescriptor: return new SubtractInstruction();
                case OpCode.Multiply:
                case OpCode.MultiplyDescriptor: return new MultiplyInstruction();
                case OpCode.Divide:
                case OpCode.DivideDescriptor: return new DivideInstruction();

                // Address Instructions
                case OpCode.Get:
                case OpCode.GetDescriptor:
                    return new GetInstruction();
                case OpCode.Store:
                case OpCode.StoreDescriptor:
                    return new StoreInstruction();

                // IO Instructions
                case OpCode.ReadFloat: return new ReadFloatInstruction();
                case OpCode.ReadInt: return new ReadIntegerInstruction();
                case OpCode.ValuePrint: return new ValuePrintInstruction();
                case OpCode.StringPrint: return new StringPrintInstruction();
                case OpCode.NewLine: return new NewLineInstruction();
                case OpCode.Space: return new SpacePrintInstruction();

                // Accumulator Instructions
                case OpCode.Halt: return new HaltInstruction();

                // Boolean Logic Instructions
                case OpCode.Negate: return new NegateInstruction();
                case OpCode.BooleanEqualTo: return new BooleanEqualToInstruction();
                case OpCode.BooleanNotEqualTo: return new BooleanNotEqualToInstruction();
                case OpCode.And:
                case OpCode.AndDescriptor: return new BooleanAndInstruction();
                case OpCode.Or:
                case OpCode.OrDescriptor: return new BooleanOrInstruction();
                case OpCode.Xor:
                case OpCode.XorDescriptor: return new BooleanXorInstruction();

                // Boolean Logic Instructions
                case OpCode.GreaterThan: return new GreaterThanInstruction();
                case OpCode.GreaterThanEqual: return new GreaterThanOrEqualInstruction();
                case OpCode.LessThan: return new LessThanInstruction();
                case OpCode.LessThanEqual: return new LessThanOrEqualInstruction();
                case OpCode.EqualTo: return new EqualToInstruction();
                case OpCode.NotEqualTo: return new NotEqualToInstruction();

                // Array Index Instructions
                case OpCode.AccumulatorToIndex: return new AccumulatorToIndexInstruction();
                case OpCode.IncrementIndex: return new IncrementIndexInstruction();
                case OpCode.DecrementIndex: return new DecrementIndexInstruction();
                case OpCode.ResetIndex: return new ResetIndexInstruction();
                case OpCode.LoadIndex: return new LoadIndexInstruction();

                default: throw new ArgumentException("Instruction is not defined for " + key, nameof(key));
            }
        }
    }

    public class CachedInstructionFactory : IFactory<OpCode, IInstruction>
    {
        private IDictionary<OpCode, IInstruction> _cachedInstructions = new Dictionary<OpCode, IInstruction>();
        private InstructionFactory _factory = new InstructionFactory();

        public IInstruction Create(OpCode key)
        {
            if (_cachedInstructions.ContainsKey(key))
                return _cachedInstructions[key];

            // Does not exist
            IInstruction inst = _factory.Create(key);
            _cachedInstructions[key] = inst;
            return inst;
        }
    }
}
