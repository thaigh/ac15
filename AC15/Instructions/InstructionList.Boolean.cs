﻿using System;
using System.Collections.Generic;
using System.Text;
using AC15.Cpu;
using AC15.Memory;

namespace AC15.Instructions
{
    public class NegateInstruction : IInstruction
    {
        public OpCode Code => OpCode.Negate;

        public IMemory EffectiveOperand {
            get => throw new InvalidOperationException();
            set => throw new InvalidOperationException();
        }

        public void Execute(IAccumulator acc) { acc.Negate(); }

        public override string ToString()
        {
            return string.Format("[NegateInstruction: Code={0}]", Code);
        }
    }

    public class BooleanEqualToInstruction : IInstruction
    {
        public OpCode Code => OpCode.BooleanEqualTo;

        private BooleanMemory _bool = new BooleanMemory();

        public IMemory EffectiveOperand
        {
            get => _bool;
            set => _bool = (BooleanMemory)value;
        }

        public bool Value
        {
            get => _bool.Value;
            set => _bool = new BooleanMemory(value);
        }

        public BooleanEqualToInstruction() : this(false) { }
        public BooleanEqualToInstruction(bool value) { this.Value = value; }

        public void Execute(IAccumulator acc) { acc.EqualTo(_bool.Value); }

        public override string ToString()
        {
            return string.Format("[BooleanEqualToInstruction: Code={0}, EffectiveOperand={1}]", Code, EffectiveOperand);
        }
    }

    public class BooleanNotEqualToInstruction : IInstruction
    {
        public OpCode Code => OpCode.BooleanNotEqualTo;

        private BooleanMemory _bool = new BooleanMemory();

        public IMemory EffectiveOperand
        {
            get => _bool;
            set => _bool = (BooleanMemory)value;
        }

        public bool Value
        {
            get => _bool.Value;
            set => _bool = new BooleanMemory(value);
        }

        public BooleanNotEqualToInstruction() : this(false) { }
        public BooleanNotEqualToInstruction(bool value) { this.Value = value; }

        public void Execute(IAccumulator acc) { acc.NotEqualTo(_bool.Value); }

        public override string ToString()
        {
            return string.Format("[BooleanNotEqualToInstruction: Code={0}, EffectiveOperand={1}]", Code, EffectiveOperand);
        }
    }

    public class BooleanAndInstruction : IInstruction
    {
        public OpCode Code => OpCode.And;

        private BooleanMemory _bool = new BooleanMemory();

        public IMemory EffectiveOperand
        {
            get => _bool;
            set => _bool = (BooleanMemory)value;
        }

        public bool Value
        {
            get => _bool.Value;
            set => _bool = new BooleanMemory(value);
        }

        public BooleanAndInstruction() : this(false) { }
        public BooleanAndInstruction(bool value) { this.Value = value; }

        public void Execute(IAccumulator acc) { acc.And(_bool.Value); }

        public override string ToString()
        {
            return string.Format("[BooleanAndInstruction: Code={0}, EffectiveOperand={1}]", Code, EffectiveOperand);
        }
    }

    public class BooleanOrInstruction : IInstruction
    {
        public OpCode Code => OpCode.Or;

        private BooleanMemory _bool = new BooleanMemory();

        public IMemory EffectiveOperand
        {
            get => _bool;
            set => _bool = (BooleanMemory)value;
        }

        public bool Value
        {
            get => _bool.Value;
            set => _bool = new BooleanMemory(value);
        }

        public BooleanOrInstruction() : this(false) { }
        public BooleanOrInstruction(bool value) { this.Value = value; }

        public void Execute(IAccumulator acc) { acc.Or(_bool.Value); }

        public override string ToString()
        {
            return string.Format("[BooleanOrInstruction: Code={0}, EffectiveOperand={1}]", Code, EffectiveOperand);
        }
    }

    public class BooleanXorInstruction : IInstruction
    {
        public OpCode Code => OpCode.Xor;

        private BooleanMemory _bool = new BooleanMemory();

        public IMemory EffectiveOperand
        {
            get => _bool;
            set => _bool = (BooleanMemory)value;
        }

        public bool Value
        {
            get => _bool.Value;
            set => _bool = new BooleanMemory(value);
        }

        public BooleanXorInstruction() : this(false) { }
        public BooleanXorInstruction(bool value) { this.Value = value; }

        public void Execute(IAccumulator acc) { acc.Xor(_bool.Value); }

        public override string ToString()
        {
            return string.Format("[BooleanXorInstruction: Code={0}, EffectiveOperand={1}]", Code, EffectiveOperand);
        }
    }

}
