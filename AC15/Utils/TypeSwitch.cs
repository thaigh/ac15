﻿using System;
using System.Collections.Generic;

namespace AC15.Utils
{
    public class ActionTypeSwitch
    {
        // Taken from: https://stackoverflow.com/a/7301514
        Dictionary<Type, Action<object>> _matches = new Dictionary<Type, Action<object>>();

        public ActionTypeSwitch Case<T>(Action<T> action) {
            _matches.Add(typeof(T), (x) => action((T)x));
            return this;
        }

        public void Switch(object x) {
            Action<object> action = _matches[x.GetType()];
            action(x);
        }
    }

    /// <summary>
    /// Creates a switch of functions with no input, that return a type T argument
    /// </summary>
    public class FuncTypeSwitch
    {
        Dictionary<Type, Func<object>> _matches = new Dictionary<Type, Func<object>>();

        public FuncTypeSwitch Case<T>(Func<T> func)
        {
            _matches.Add(typeof(T), () => func());
            return this;
        }

        public T Switch<T>()
        {
            Func<object> func = _matches[typeof(T)];
            object retVal = func();
            return (T)retVal;
        }
    }

}
