using System;
using AC15.Memory;

namespace AC15.Exceptions
{
    public class InvalidMemoryTypeException : Exception
    {

        public InvalidMemoryTypeException() : this(string.Format("Invalid Memory Type found")) { }

        public InvalidMemoryTypeException(string message) : base(message) { }

    }
}
