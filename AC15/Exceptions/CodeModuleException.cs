﻿using System;
namespace AC15.Exceptions
{
    public class CodeModuleException : Exception
    {
        public CodeModuleException() : this("Error ocurred when loading code module") { }
        public CodeModuleException(string message) : base(message) { }
    }
}
