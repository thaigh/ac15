﻿using System;
namespace AC15.Exceptions
{
    public class AddressOutOfBoundsException : Exception
    {
        public readonly int LowerBound;
        public readonly int UpperBound;
        public readonly int AttemptedAddress;

        public AddressOutOfBoundsException(
            int lowerBound, int upperBound, int attemptedAddress)
            : this (lowerBound, upperBound, attemptedAddress,
                string.Format("Attempted to access Address {2} that is out of bounds [{0}, {1}]", lowerBound, upperBound, attemptedAddress))
        { }

        public AddressOutOfBoundsException (
            int lowerBound, int upperBound, int attemptedAddress, string message)
            : base(message)
        {
            this.LowerBound = lowerBound;
            this.UpperBound = upperBound;
            this.AttemptedAddress = attemptedAddress;
        }
    }

    public static class AddressBoundary {
        public static void ValidateAddress(int attemptedAddress, int lowerBound, int upperBound, string boundaryName) {

            if (upperBound < lowerBound)
                throw new ArgumentException("Upper Bound must be at greater than (or equal to) lower bound");

            if (attemptedAddress < lowerBound || attemptedAddress > upperBound)
                throw new AddressOutOfBoundsException(
                    lowerBound, upperBound, attemptedAddress,
                    string.Format("Address {0} is out of bounds [{1}, {2}] for Boundary Name {3}",
                                 attemptedAddress, lowerBound, upperBound, boundaryName));
        }
    }
}
