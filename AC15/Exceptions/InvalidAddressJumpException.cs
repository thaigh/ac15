﻿using System;
namespace AC15.Exceptions
{
    public class InvalidAddressJumpException : Exception
    {
        public InvalidAddressJumpException() : this("Cannot jump to address") { }
        public InvalidAddressJumpException(string message) : base(message) { }
    }
}
