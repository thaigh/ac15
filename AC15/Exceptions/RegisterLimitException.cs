﻿using System;
namespace AC15.Exceptions
{
    public class RegisterLimitException : Exception
    {
        public RegisterLimitException()
            : this("Maximum number of items in this register have been registered") { }

        public RegisterLimitException(string message) : base(message) { }
    }
}
