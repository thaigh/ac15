﻿using System;
namespace AC15.Exceptions
{
    public class OperandException : Exception
    {
        public OperandException() : this("Invalid operand") { }
        public OperandException(string message) : base(message) { }
    }
}
