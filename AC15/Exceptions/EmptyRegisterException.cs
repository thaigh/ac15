﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AC15.Exceptions
{
    public class EmptyRegisterException : Exception
    {
        public readonly int Address;

        public EmptyRegisterException(int address) : this(address, "Cannot access address of empty register") { }
        public EmptyRegisterException(int address, string message) : base(message) { Address = address; }
    }
}
