﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AC15.Exceptions
{
    public class IOStreamReadException : Exception
    {
        public readonly string TokenString;

        public IOStreamReadException(string tokenString) : this(tokenString, "Unable to read from stream. Next token is invalid: {0}") { }
        public IOStreamReadException(string tokenString, string message) : base(message)
        {
            TokenString = tokenString;
        }
    }
}
