﻿using System;
using AC15.Memory;

namespace AC15.Exceptions
{
    public class InvalidMemoryCastException : Exception
    {
        public readonly MemoryType Expected;
        public readonly MemoryType Actual;

        public InvalidMemoryCastException(MemoryType expected, MemoryType actual)
            : this(expected, actual, string.Format("Invalid Memory Cast. Expected {0} but got {1}", expected, actual))
        { }
        
        public InvalidMemoryCastException(MemoryType expected, MemoryType actual, string message) : base(message)
        {
            this.Expected = expected;
            this.Actual = actual;
        }

    }
}
