﻿using System;

namespace AC15.Exceptions
{
    public class InvalidWordLengthException : Exception
    {

        public InvalidWordLengthException() : this("Invalid Word Length") { }
        public InvalidWordLengthException(string message) : base(message) { }

    }
}
