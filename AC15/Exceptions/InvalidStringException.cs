﻿using System;
namespace AC15.Exceptions
{
    public class InvalidStringException : Exception
    {
        public InvalidStringException() : this("Invalid format for String Memory") { }
        public InvalidStringException(string message) : base(message) { }
    }
}
