﻿using System;
using AC15.Instructions;
namespace AC15.Cpu
{
    public interface ICpu
    {
        void Cycle();
        void Fetch();
        IInstruction Decode(OpCode code);
        void Execute(IInstruction instruction);
    }
}
