﻿using System;
using System.IO;
using AC15.Exceptions;
using AC15.Memory;
using StreamTokeniser;

namespace AC15.Cpu
{
    public partial class AccumulatorCpu : IAccumulator
    {
        #region AccumulatorOperations

        public void Halt() { /* Do nothing */ }
        public void NoOperation() { /* Do nothing */ }
        public void Clear() { Accumulator.Value = 0; }

        #endregion

        #region MathOperations

        public void Set(double val) { Accumulator.Value = val; }
        public void ChangeSign()    { Accumulator.Value = Accumulator.Value.GetFloatingPoint() * -1; }
        public void AbsoluteValue() { Accumulator.Value = Math.Abs(Accumulator.Value.GetFloatingPoint()); }

        public void Add(double val)        { Accumulator.Value = Accumulator.Value.GetFloatingPoint() + val; }
        public void Subtract(double val)   { Accumulator.Value = Accumulator.Value.GetFloatingPoint() - val; }
        public void Multiply(double val)   { Accumulator.Value = Accumulator.Value.GetFloatingPoint() * val; }
        public void Divide(double val)     { Accumulator.Value = Accumulator.Value.GetFloatingPoint() / val; }
        public void IntegerDivide(int val) { Accumulator.Value = (int)(Accumulator.Value.GetFloatingPoint() / val); }

        public void Power(double val) { Accumulator.Value = Math.Pow(Accumulator.Value.GetFloatingPoint(), val); }
        public void Sqrt()            { Accumulator.Value = Math.Sqrt(Accumulator.Value.GetFloatingPoint()); }
        public void Root(double root) { Accumulator.Value = Math.Pow(Accumulator.Value.GetFloatingPoint(), 1.0 / root); }

        #endregion

        #region AddressOperations


        private void ValidateMemory(MemoryType type) {
            if (type != MemoryType.FloatingPoint && type != MemoryType.Boolean)
                throw new InvalidMemoryTypeException();
        }

        public void GetMemoryFromAddress(int address)
        {
            IMemory mem = RegisterGroup.Ram.Get(address);

            // Assert Data Type for Operation
            ValidateMemory(mem.MemoryType);

            this.Accumulator.Value.Set(mem);
        }

        public void StoreMemoryToAddress(int address)
        {
            IMemory mem = this.Accumulator.Value.Get();

            // Assert Data Type for Operation
            ValidateMemory(mem.MemoryType);

            RegisterGroup.Ram.Set(address, mem);
        }

        public void GetMemoryAsAddressPointer(int address)
        {
            this.Accumulator.Value.SetAddress(address);
        }

        public void GetArrayDescriptorAtAddress(int address)
        {
            // Won't work with this implementation because of how RAM Memory is separate from ArrayDescriptors
            throw new NotImplementedException();
        }

        public void GetArrayDescriptorForArray(int arrayId)
        {

        }



        public void StoreAccumulatorDirectlyToAddress(int address)
        {

        }


        #endregion

        #region IoOperations

        public void ReadFloat()
        {
            Tokeniser st = new Tokeniser(this.InputStream);

            if (!st.HasNext<double>())
                throw new IOStreamReadException(st.Peek());

            double num = st.ReadNext<double>();
            this.Accumulator.Value = num;
        }

        public void ReadInteger()
        {
            Tokeniser st = new Tokeniser(this.InputStream);

            if (!st.HasNext<int>())
                throw new IOStreamReadException(st.Peek());

            int num = st.ReadNext<int>();
            this.Accumulator.Value = num;
        }

        public void PrintAccumulatorValue() { this.OutputStream.Write(Accumulator.Value.ToString()); }
        public void PrintString(string s)   { this.OutputStream.Write(s); }
        public void PrintChar(char c)       { this.OutputStream.Write(c); }
        public void PrintNewLine()          { this.OutputStream.WriteLine(); }
        public void PrintSpace()            { this.OutputStream.Write(' '); }

        #endregion

        #region BooleanLogicOperations

        public void Negate()                { this.Accumulator.Value = !this.Accumulator.Value.GetBoolean(); }
        public void Set(bool val)           { this.Accumulator.Value = val; }
        public void EqualTo(bool val)       { this.Accumulator.Value = this.Accumulator.Value.GetBoolean() == val; }
        public void NotEqualTo(bool val)    { this.Accumulator.Value = this.Accumulator.Value.GetBoolean() != val; }
        public void And(bool val)           { this.Accumulator.Value = this.Accumulator.Value.GetBoolean() && val; }
        public void Or(bool val)            { this.Accumulator.Value = this.Accumulator.Value.GetBoolean() || val; }
        public void Xor(bool val)           { this.Accumulator.Value = this.Accumulator.Value.GetBoolean() ^ val; }

        #endregion

        #region IMathBooleanLogicMachine

        public void GreaterThan(double val = 0)         { this.Accumulator.Value = this.Accumulator.Value.GetFloatingPoint() > val; }
        public void GreaterThanOrEqual(double val = 0)  { this.Accumulator.Value = this.Accumulator.Value.GetFloatingPoint() >= val; }
        public void LessThan(double val = 0)            { this.Accumulator.Value = this.Accumulator.Value.GetFloatingPoint() < val; }
        public void LessThanOrEqual(double val = 0)     { this.Accumulator.Value = this.Accumulator.Value.GetFloatingPoint() <= val; }
        public void EqualTo(double val = 0)             { this.Accumulator.Value = this.Accumulator.Value.GetFloatingPoint() == val; }
        public void NotEqualTo(double val = 0)          { this.Accumulator.Value = this.Accumulator.Value.GetFloatingPoint() != val; }

        #endregion

        #region IArrayMachine

        public void ResetIndex()                        { SetIndex(0); }
        public void SetIndex(int index)                 { this.ArrayIndexRegister.ArrayIndex = index; }
        public void IncrementIndex()                    { this.ArrayIndexRegister.ArrayIndex++; }
        public void DecrementIndex()                    { this.ArrayIndexRegister.ArrayIndex--; }
        public void LoadIndexFromAddress(int address)   { SetIndex((int)this.RegisterGroup.Ram.GetFloatingPoint(address).Value); }
        public void LoadIndexFromAccumulator()          { SetIndex((int)this.Accumulator.Value.GetFloatingPoint()); }

        #endregion

    }
}
