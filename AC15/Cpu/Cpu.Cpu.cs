﻿using System;
using AC15.Registers;
using AC15.Instructions;
using AC15.File;
using AC15.Memory;
using System.Collections.Generic;
using System.IO;

namespace AC15.Cpu
{
    public partial class AccumulatorCpu : ICpu
    {
        public readonly AccumulatorRegister Accumulator;
        public readonly OpCodeRegister OpCodeRegister;
        public readonly ProgramCounter ProgramCounter;
        public readonly ArrayIndexRegister ArrayIndexRegister;
        public RegisterGroup RegisterGroup { get; private set; }

        private CachedInstructionFactory _instructionFactory = new CachedInstructionFactory();

        public TextReader InputStream { get; set; }
        public TextWriter OutputStream { get; set; }

        public AccumulatorCpu() {
            this.Accumulator = new AccumulatorRegister();
            this.OpCodeRegister = new OpCodeRegister();
            this.ProgramCounter = new ProgramCounter();
            this.ArrayIndexRegister = new ArrayIndexRegister();
            this.RegisterGroup = new RegisterGroup();

            InputStream = Console.In;
            OutputStream = Console.Out;
        }

        public void LoadProgramFromFile(string filePath) {
            CodeModuleReader reader = new CodeModuleReader();
            this.RegisterGroup = reader.LoadCodeModuleFromFile(filePath);
        }

        public void LoadProgramFromText(string program) {
            CodeModuleReader reader = new CodeModuleReader();
            this.RegisterGroup = reader.LoadCodeModuleFromText(program);
        }

        public void LoadRegisterGroup(RegisterGroup registers) {
            this.RegisterGroup = registers;
        }

        public void Run() {
            while (OpCodeRegister.OpCode != (int)OpCode.Halt)
                Cycle();
        }

        public void Cycle()
        {
            Fetch();
            IInstruction instruction = Decode((OpCode)OpCodeRegister.OpCode);
            Execute(instruction);
        }

        public void Fetch()
        {
            Console.WriteLine("Fetch Next Instruction...");

            OpCodeRegister.Clear();

            int opCode = FetchNextInstructionByte();
            OpCodeRegister.OpCode = opCode;

            if (OpCodeRegister.SingleInstruction) return;
            int[] operands = GetOperandsForOpCode();

            int operand = OpCodeRegister.OperandFromBytes(operands);
            OpCodeRegister.Operand = operand;

            Console.WriteLine("Instruction: {0} - Operands: [{1}]", opCode, string.Join(", ", operands));
        }

        private int[] GetOperandsForOpCode() {
            List<int> operands = new List<int>();
            operands.Add(FetchNextInstructionByte());
            operands.Add(FetchNextInstructionByte());

            if (!OpCodeRegister.DescriptorOpCode)
            {
                operands.Add(FetchNextInstructionByte());
                operands.Add(FetchNextInstructionByte());
            }

            return operands.ToArray();
        }

        private int FetchNextInstructionByte() {
            InstructionMemory mem = this.RegisterGroup.Instructions.Get(ProgramCounter.Word);
            int nextByte = mem.Bytes[ProgramCounter.Byte];

            ProgramCounter.Increment();

            return nextByte;
        }

        public IInstruction Decode(OpCode code)
        {
            Console.WriteLine("Decode Instruction from OpCode {0}", code);

            IInstruction instruction = _instructionFactory.Create(code);
            //instruction.EffectiveOperand = OpCodeRegister.EffectiveOperand;
            SetEffectiveOperand(instruction);
            return instruction;
        }

        public void Execute(IInstruction instruction)
        {
            Console.WriteLine("Execute Instruction {0} with Effective Operand {1}", instruction.Code, instruction.EffectiveOperand);

            instruction.Execute(this);

            Console.WriteLine("Accumulator State: {0}", Accumulator);
        }

        private void SetEffectiveOperand(IInstruction instruction) {
            var associatedDataType = ((OpCode)OpCodeRegister.OpCode).AssociatedDataType();
            switch (associatedDataType)
            {
                case OpCodeDataType.SingleInstruction: break;
                case OpCodeDataType.FloatingPoint: instruction.EffectiveOperand = GenericMemoryFromOperand(); break;
                case OpCodeDataType.Boolean:       instruction.EffectiveOperand = GenericMemoryFromOperand(); break;
                case OpCodeDataType.GenericMemory: instruction.EffectiveOperand = GenericMemoryFromOperand(); break;
                case OpCodeDataType.Address:       instruction.EffectiveOperand = ResolveAddressFromOperand(); break;
                //case OpCodeDataType.Subprogram,
                //case OpCodeDataType.Other
                default: break;
            }
        }

        private IMemory GenericMemoryFromOperand() {
            int address = OpCodeRegister.Operand;

            if (OpCodeRegister.DescriptorOpCode) {
                // Operand is the Array number
                // Array Index Register contains the array element index
                ArrayDescriptor desc = RegisterGroup.ArrayDescriptors.Get(address);
                int index = ArrayIndexRegister.ArrayIndex;

                IMemory arrayMem = RegisterGroup.Ram.GetFromArray(desc, index);
                return arrayMem;
            }

            // Lookup address in RAM
            IMemory mem = RegisterGroup.Ram.Get(address);

            // Handle Address Resolution
            while (mem.MemoryType == MemoryType.MemoryAddress) {
                MemoryAddress addr = (MemoryAddress)mem;
                mem = RegisterGroup.Ram.Get(addr.AddressIndex);
            }

            return mem;
        }

        private MemoryAddress ResolveAddressFromOperand() {
            int address = OpCodeRegister.Operand;

            if (OpCodeRegister.DescriptorOpCode) {
                // Operand is the Array number
                // Array Index Register contains the array element index
                ArrayDescriptor desc = RegisterGroup.ArrayDescriptors.Get(address);
                return new MemoryAddress(desc.ArrayStartAddress + ArrayIndexRegister.ArrayIndex);
            }

            return new MemoryAddress(address);
        }

    }
}
