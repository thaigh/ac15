﻿using System;
using System.Reflection.Metadata;
using System.IO;
namespace AC15.Cpu
{
    public interface IAccumulator : IMathematicalMachine, IAddressableMachine, IInputOutputMachine, IBooleanLogicMachine, IMathBooleanLogicMachine,
                                    IArrayMachine
    {
        /// <summary>
        /// Triggers the accumulator to stop execution
        /// </summary>
        void Halt();

        /// <summary>
        /// Performs no operation. Used to manually pad instructions in byte-code
        /// </summary>
        void NoOperation();

        /// <summary>
        /// Clears the primary accumulator register
        /// </summary>
        void Clear();
    }

    public interface IMathematicalMachine {

        /// <summary>
        /// Changes the sign of the value in the primary register
        /// </summary>
        void ChangeSign();

        /// <summary>
        /// Sets the primary register to the the Absolute Value of the primary register
        /// </summary>
        void AbsoluteValue();


        /// <summary>
        /// Set the primary register to the value specified
        /// </summary>
        /// <param name="val">The value to set in the primary register</param>
        void Set(double val);


        /// <summary>
        /// Adds the specified value to the primary register
        /// </summary>
        /// <param name="val">The value to add to the primary register</param>
        void Add(double val);

        /// <summary>
        /// Subtracts the specified value from the primary register
        /// </summary>
        /// <param name="val">The value to subtract from the primary register</param>
        void Subtract(double val);

        /// <summary>
        /// Multiplies the specified value with the primary register
        /// </summary>
        /// <param name="val">The value to multiply with the primary register</param>
        void Multiply(double val);

        /// <summary>
        /// Divides the primary register by the specified value
        /// </summary>
        /// <param name="val">The value to divide the primary register by</param>
        void Divide(double val);

        /// <summary>
        /// Integer Divides the primary register by the specified value
        /// </summary>
        /// <param name="val">The value to integer divide the primary register by</param>
        void IntegerDivide(int val);


        /// <summary>
        /// Raises the primary register to the power of the specified value
        /// </summary>
        /// <param name="val">The value to raise the primary register to the power of</param>
        void Power(double val);

        /// <summary>
        /// Sets the primary register to the square root of the value stored in the primary register
        /// </summary>
        void Sqrt();

        /// <summary>
        /// Sets the primary register to the nth root of the value stored in the primary register
        /// </summary>
        /// <param name="root">The root</param>
        void Root(double root);
    }

    public interface IBooleanLogicMachine {

        /// <summary>
        /// Negates the value of the primary register
        /// </summary>
        void Negate();

        /// <summary>
        /// Sets the primary register to the specified value
        /// </summary>
        /// <param name="val">The value to set in the primary register</param>
        void Set(bool val);


        /// <summary>
        /// Checks to see if the primary register is equal to the specified value.
        /// The primary register will store the result of this function
        /// </summary>
        /// <param name="val">The value to compare with</param>
        void EqualTo(bool val);

        /// <summary>
        /// Checks to see if the primary register is not equal to the specified value.
        /// The primary register will store the result of this function
        /// </summary>
        /// <param name="val">The value to compare with</param>
        void NotEqualTo(bool val);


        /// <summary>
        /// Performs the boolean AND function between the primary register and the specified value.
        /// The primary register will store the result of this function
        /// </summary>
        /// <param name="val">The value to AND with</param>
        void And(bool val);

        /// <summary>
        /// Performs the boolean OR function between the primary register and the specified value.
        /// The primary register will store the result of this function
        /// </summary>
        /// <param name="val">The value to OR with</param>
        void Or(bool val);

        /// <summary>
        /// Performs the boolean XOR function between the primary register and the specified value.
        /// The primary register will store the result of this function
        /// </summary>
        /// <param name="val">The value to XOR with</param>
        void Xor(bool val);
    }

    public interface IMathBooleanLogicMachine {

        /// <summary>
        /// Checks to see if the primary register is greater than the specified value.
        /// The primary register will store the result of this function
        /// </summary>
        /// <param name="val">The value to compare with.</param>
        void GreaterThan(double val = 0);

        /// <summary>
        /// Checks to see if the primary register is greater than or equal to the specified value.
        /// The primary register will store the result of this function
        /// </summary>
        /// <param name="val">The value to compare with.</param>
        void GreaterThanOrEqual(double val = 0);

        /// <summary>
        /// Checks to see if the primary register is less than the specified value.
        /// The primary register will store the result of this function
        /// </summary>
        /// <param name="val">The value to compare with.</param>
        void LessThan(double val = 0);

        /// <summary>
        /// Checks to see if the primary register is less than or equal to the specified value.
        /// The primary register will store the result of this function
        /// </summary>
        /// <param name="val">The value to compare with.</param>
        void LessThanOrEqual(double val = 0);


        /// <summary>
        /// Checks to see if the primary register is equal to the specified value.
        /// The primary register will store the result of this function
        /// </summary>
        /// <param name="val">The value to compare with.</param>
        void EqualTo(double val = 0);

        /// <summary>
        /// Checks to see if the primary register is not equal to the specified value.
        /// The primary register will store the result of this function
        /// </summary>
        /// <param name="val">The value to compare with.</param>
        void NotEqualTo(double val = 0);
    }

    public interface IInputOutputMachine {
        void ReadFloat();
        void ReadInteger();

        void PrintAccumulatorValue();
        void PrintString(string s);
        void PrintChar(char c);
        void PrintNewLine();
        void PrintSpace();
    }

    public interface IArrayMachine {

        // All methods will implicitly use the Index Register to index into the provided array


        /// <summary>
        /// Resets the value of the Array Index Register back to 0
        /// </summary>
        void ResetIndex();

        /// <summary>
        /// Sets the value of the Array Index Register to the specified value
        /// </summary>
        /// <param name="index">The index value to store in the Array Index Register</param>
        void SetIndex(int index);

        /// <summary>
        /// Increments the index stored in the Array Index Register
        /// </summary>
        void IncrementIndex();

        /// <summary>
        /// Decrements the index stored in the Array Index Register
        /// </summary>
        void DecrementIndex();

        /// <summary>
        /// Loads the Array Index Register to the (integer) value at the given RAM Memory address
        /// </summary>
        /// <param name="address">The address to get a numeric value and load into the Array Index Register</param>
        void LoadIndexFromAddress(int address);

        /// <summary>
        /// Sets the value of the Array Index Register to the value of the Accumulator's Primary Register
        /// </summary>
        void LoadIndexFromAccumulator();


    }

    public interface IAddressableMachine {

        /// <summary>
        /// Gets the RAM Memory element at the speficied address and stores in the primary register
        /// </summary>
        /// <param name="address">The address to load into the primary register</param>
        void GetMemoryFromAddress(int address); // GET-66 GET-DESC-67

        /// <summary>
        /// Stores the value of the primary register into the RAM Memory element at the specified address
        /// </summary>
        /// <param name="address">The address to store memory from the primary register</param>
        void StoreMemoryToAddress(int address); // STORE-68 STORE-DESC-69


        /// <summary>
        /// Gets the RAM Memory element at the specified address and stores an Address Descriptor for the address
        /// in the primary register
        /// </summary>
        /// <param name="address">The address to get the descriptor for</param>
        void GetMemoryAsAddressPointer(int address); // GET-ADDRESS-84 GET-ADDRESS-DESC-85


        /// <summary>
        /// Gets the Descriptor at address and stores it in the primary register.
        /// Assumes that the address provided will be an array descriptor.
        /// Gets copy of DESC from memory (parameter)
        /// </summary>
        /// <param name="address">The address at which an array descriptor exists</param>
        void GetArrayDescriptorAtAddress(int address); // GET-DESC-86

        /// <summary>
        /// Gets the Array Descriptor for the specified array.
        /// Get copy of DESC from Array Desc Area
        /// </summary>
        /// <param name="arrayId">The array descriptor identifier</param>
        void GetArrayDescriptorForArray(int arrayId); // GET-DESC-DESC-87


        /// <summary>
        /// Stores the value of the Accumulator's Primary Register directly at the address specified
        /// without the use of Auto-Indirect address resolution
        /// </summary>
        /// <param name="address"></param>
        void StoreAccumulatorDirectlyToAddress(int address); // STORE_DIRECT-88
    }

    public interface ProgrammableMachine {

        /// <summary>
        /// Calls the subprocedure with the specified id.
        /// Places the Program Counter at the start of the subprocedure executable code block
        /// ready for instruction execution on the next CPU cycle.
        /// Saves the current Program Counter reference into the Subprogram Descriptor to allow return
        /// of execution in the current routine
        /// </summary>
        /// <param name="subprocId">The Subprocedure identifier</param>
        void CallSubprocedure(int subprocId);

        /// <summary>
        /// Returns from the subprocedure with the specified id back to the parent calling routine.
        /// Places the Program Counter at the return address as saved when calling current routine.
        /// Clears the Subprogram Descriptor's return address
        /// </summary>
        /// <param name="subprocId">Subproc identifier.</param>
        void ReturnFromSubprocedure(int subprocId);


        /// <summary>
        /// Performs the Branch Forward function if the value of the primary register resolves to true
        /// </summary>
        /// <param name="branchLength">The Branch Length for the number of bytes to jump by</param>
        void ConditionalBranchForward(int branchLength);

        /// <summary>
        /// Performs the Branch Back function if the value of the primary register resolves to true
        /// </summary>
        /// <param name="branchLength">The Branch Length for the number of bytes to jump by</param>
        void ConditionalBranchBack(int branchLength);


        /// <summary>
        /// Moves the Program Counter forward by the specified number of bytes
        /// </summary>
        /// <param name="branchLength">The Branch Length for the number of bytes to jump by</param>
        void BranchForward(int branchLength);

        /// <summary>
        /// Moves the Program Counter backward by the specified number of bytes
        /// </summary>
        /// <param name="branchLength">The Branch Length for the number of bytes to jump by</param>
        void BranchBack(int branchLength);
    }

}
