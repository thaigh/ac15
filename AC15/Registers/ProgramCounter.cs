﻿using System;
using AC15.Exceptions;
namespace AC15.Registers
{
    public class ProgramCounter
    {
        public const int WordLength = 8;

        public int Word { get; private set; }
        public int Byte { get; private set; }

        public int FullAddress { get { return (Word * WordLength) + Byte;  } }

        public ProgramCounter() : this(0, 0) { }

        public ProgramCounter(int word, int bytes) {
            this.Word = word;
            this.Byte = bytes;
        }

        public ProgramCounter(int fullAddress) {

            int word = fullAddress / WordLength;
            int bytes = fullAddress % WordLength;

            this.Word = word;
            this.Byte = bytes;
        }

        public void Increment()
        {
            Byte++;

            // If Byte = 8, then we are at Word + 1, Byte 0
            if (Byte == WordLength) {
                Word++;
                Byte = 0;
            }
        }

        public void Jump(int bytes)
        {
            int target = FullAddress + bytes;
            if (target < 0) throw new InvalidAddressJumpException("Cannot jump below 0");

            ProgramCounter pc = new ProgramCounter(target);
            this.Word = pc.Word;
            this.Byte = pc.Byte;
        }

        public override string ToString ()
        {
            return string.Format("[ProgramCounter: Word={0}, Byte={1}]", Word, Byte);
        }

    }
}
