﻿using System;
namespace AC15.Registers
{
    public class RegisterGroup
    {
        public readonly Ram Ram = new Ram();
        public readonly InstructionSet Instructions = new InstructionSet();
        public readonly ArrayDescriptorRegister ArrayDescriptors = new ArrayDescriptorRegister();
        public readonly SubprogramDescriptorRegister SubprogramDescriptors = new SubprogramDescriptorRegister();


        public RegisterGroup() { }

        public RegisterGroup(Ram ram, InstructionSet inst, ArrayDescriptorRegister arr, SubprogramDescriptorRegister subp) {
            this.Ram = ram;
            this.Instructions = inst;
            this.ArrayDescriptors = arr;
            this.SubprogramDescriptors = subp;
        }
    }
}
