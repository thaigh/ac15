﻿using System;
using System.Collections.Generic;
using AC15.Exceptions;
using AC15.Memory;
using System.Net.Sockets;

namespace AC15.Registers
{
    public class Ram
    {
        public const int MemoryWordSpace = 4096;

        private List<IMemory> _memoryBlock = new List<IMemory>();

        public int Size { get { return _memoryBlock.Count; } }

        public int NextAddress { get { return Size; }}

        public IMemory this[int i] {
            get { return Get(i); }
            set { Set(i, value); }
        }

        #region AddMethods

        public void Add(IMemory memory)
        {
            PreconditionCheckForSizeLimit();
            _memoryBlock.Add(memory);
        }

        public void AddFloatingPoint(double val) {
            FloatingPointMemory mem = new FloatingPointMemory(val);
            AddFloatingPoint(mem);
        }

        public void AddFloatingPoint(FloatingPointMemory mem) {
            PreconditionCheckForSizeLimit();
            _memoryBlock.Add(mem);
        }

        public void AddArrayBlock(ArrayDescriptor block)
        {
            for (int i = 0; i < block.ArraySize; i++)
            {
                AddFloatingPoint(new FloatingPointMemory(FloatingPointMemory.DefaultValue));
            }
        }

        public void AddString(char[] chars) {
            StringMemory mem = new StringMemory(chars);
            AddString(mem);
        }

        public void AddString(StringMemory mem) {
            PreconditionCheckForSizeLimit();
            _memoryBlock.Add(mem);
        }

        #endregion

        #region GetMethods

        public IMemory Get(int address)
        {
            PreconditionCheckForAddressBounds(address);
            return _memoryBlock[address];
        }

        public FloatingPointMemory GetFloatingPoint(int address)
        {
            PreconditionCheckForAddressBounds(address);
            PreconditionCheckForCorrectMemoryType(address, MemoryType.FloatingPoint);
            return (FloatingPointMemory)_memoryBlock[address];
        }

        public IMemory GetFromArray(ArrayDescriptor desc, int arrayIndex)
        {
            int address = desc.ArrayStartAddress + arrayIndex;
            return Get(address);
        }

        public FloatingPointMemory GetFloatingPointFromArray(ArrayDescriptor desc, int arrayIndex) {
            int address = desc.ArrayStartAddress + arrayIndex;
            return GetFloatingPoint(address);
        }

        #endregion

        #region SetMethods

        public void Set(int address, IMemory value)
        {
            PreconditionCheckForAddressBounds(address);
            _memoryBlock[address] = value;
        }

        public void SetFloatingPoint(int address, double value)
        {
            PreconditionCheckForAddressBounds(address);
            FloatingPointMemory mem = new FloatingPointMemory(value);
            _memoryBlock[address] = mem;
        }

        public void SetToArray(ArrayDescriptor desc, int arrayIndex, IMemory value)
        {
            int address = desc.ArrayStartAddress + arrayIndex;
            Set(address, value);
        }

        public void SetFloatingPointToArray(ArrayDescriptor desc, int arrayIndex, double value)
        {
            int address = desc.ArrayStartAddress + arrayIndex;
            SetFloatingPoint(address, value);
        }

        #endregion

        #region PreconditionChecks

        private void PreconditionCheckForSizeLimit()
        {
            if (Size >= MemoryWordSpace)
                throw new RegisterLimitException("Maximum number of RAM Address Words have been registered");
        }

        private void PreconditionCheckForAddressBounds(int address) {
            if (Size < 1) throw new EmptyRegisterException(address);
            AddressBoundary.ValidateAddress(address, 0, Size - 1, "RAM Memory Block");
        }

        private void PreconditionCheckForCorrectMemoryType(int address, MemoryType type)
        {
            var memory = _memoryBlock[address];
            if (memory.MemoryType != type)
                throw new InvalidMemoryCastException(type, memory.MemoryType);
        }

        #endregion
    }
}
