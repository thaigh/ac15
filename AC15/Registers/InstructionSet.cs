﻿using System;
using System.Collections.Generic;
using AC15.Memory;
using AC15.Exceptions;
namespace AC15.Registers
{
    public class InstructionSet
    {
        public const int InstructionWordSpace = 4096;

        private List<InstructionMemory> _instructions = new List<InstructionMemory>();
        public int Size { get { return _instructions.Count; } }
        public int NextAddress { get { return Size; } }

        public void Add(InstructionMemory instruction) {
            PreconditionCheckForSizeLimit();
            _instructions.Add(instruction);
        }

        public void Add(int[] byteData) {
            Add(new InstructionMemory(byteData));
        }

        public InstructionMemory Get(int address) {
            PreconditionCheckForAddressBounds(address);
            return _instructions[address];
        }

        public void Set(int address, InstructionMemory value) {
            PreconditionCheckForAddressBounds(address);
            _instructions[address] = value;
        }

        public void Set(int address, int[] byteData) {
            InstructionMemory mem = new InstructionMemory(byteData);
            Set(address, mem);
        }

        public InstructionMemory this[int i] {
            get { return Get(i); }
            set { Set(i, value); }
        }

        private void PreconditionCheckForSizeLimit() {
            if (Size >= InstructionWordSpace)
                throw new RegisterLimitException("Maximum number of Instructions have been registered");
        }

        private void PreconditionCheckForAddressBounds(int address) {
            if (Size < 1) throw new EmptyRegisterException(address);
            AddressBoundary.ValidateAddress(address, 0, Size - 1, "Instruction Set");
        }
    }
}
