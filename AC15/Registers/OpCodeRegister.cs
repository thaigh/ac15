﻿using System;
using System.Collections.Generic;
using AC15.Exceptions;

namespace AC15.Registers
{
    public class OpCodeRegister
    {
        public int OpCode { get; set; }
        public int Operand { get; set; }

        public bool InvalidState => OpCode < 0;
        public bool SingleInstruction { get { CheckRegisterState(); return OpCode < 40; } }
        public bool DescriptorOpCode  { get { CheckRegisterState(); return OpCode > 40 && OpCode % 2 == 1; } }

        public OpCodeRegister()
        {
            Clear();
        }

        public void Clear() {
            this.OpCode = -1;
            this.Operand = -1;
        }

        private void CheckRegisterState()
        {
            if (InvalidState)
                throw new InvalidOperationException("OpCode Register is in Invalid State");
        }

        public override string ToString()
        {
            return string.Format (
                "[OpCodeRegister: OpCode={0}, Operand={1}]",
                OpCode, Operand
            );
        }

        private const int ByteMultiplier = 256;

        public static int OperandFromBytes(int[] bytes) {
            int operand = 0;
            foreach (var b in bytes) {
                if (b < 0)
                    throw new OperandException("Byte data cannot be negative");
                if (b >= ByteMultiplier)
                    throw new OperandException("Byte data cannot be greater than " + ByteMultiplier);
                
                operand = operand * ByteMultiplier + b;
            }
            return operand;
        }

        public static int[] BytesFromOperand(int operand) {
            if (operand < 0)
                throw new OperandException("Operand cannot be negative");

            // Special Case: Operand = 0
            if (operand == 0) return new int[] { 0 };

            List<int> bytes = new List<int>();
            while (operand / ByteMultiplier > 0) {

                // Operand = (q * Multiplier) + r
                var q = operand / ByteMultiplier;
                var r = operand % ByteMultiplier;

                bytes.Add(r);
                operand = q;
            }

            if (operand > 0)
                bytes.Add(operand);

            bytes.Reverse();
            return bytes.ToArray();
        }
    }

}
