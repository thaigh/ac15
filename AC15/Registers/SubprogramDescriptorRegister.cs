﻿using System;
using System.Collections.Generic;
using AC15.Exceptions;
using AC15.Memory;
namespace AC15.Registers
{
    public class SubprogramDescriptorRegister
    {
        public const int MaxSubProgramDescriptors = 10;
        public int Size { get { return _subprogramDescriptors.Count; } }
        private List<SubprogramDescriptor> _subprogramDescriptors = new List<SubprogramDescriptor>();

        public SubprogramDescriptor this[int i] {
            get { return Get(i); }
            set { Set(i, value); }
        }

        public SubprogramDescriptor Get(int address) {
            PreconditionCheckForAddressBounds(address);
            return _subprogramDescriptors[address];
        }

        public void Add(SubprogramDescriptor descritptor) {
            PreconditionCheckForSizeLimit();
            _subprogramDescriptors.Add(descritptor);
        }

        public void Set(int address, SubprogramDescriptor descriptor) {
            PreconditionCheckForAddressBounds(address);
            _subprogramDescriptors[address] = descriptor;
        }

        private void PreconditionCheckForSizeLimit() {
            if (Size >= MaxSubProgramDescriptors)
                throw new RegisterLimitException("Maximum number of Subprogram Descriptors have been registered");
        }

        private void PreconditionCheckForAddressBounds(int address) {
            if (Size < 1) throw new EmptyRegisterException(address);
            AddressBoundary.ValidateAddress(address, 0, Size - 1, "Subprogram Descriptor");
        }

    }
}
