﻿using System;
using System.Collections.Generic;
using AC15.Memory;
using AC15.Exceptions;

namespace AC15.Registers
{
    public class ArrayIndexRegister {
        public int ArrayIndex { get; set; }
    }

    public class ArrayDescriptorRegister
    {
        public const int MaxArrayDescriptors = 100;
        public int Size { get { return _arrayDescriptors.Count; } }
        private List<ArrayDescriptor> _arrayDescriptors = new List<ArrayDescriptor>();

        public ArrayDescriptor Get(int address) {
            PreconditionCheckForAddressBounds(address);
            return _arrayDescriptors[address];
        }

        public void Add(ArrayDescriptor descritptor) {
            PreconditionCheckForSizeLimit();
            _arrayDescriptors.Add(descritptor);
        }

        public void Set(int address, ArrayDescriptor descriptor) {
            PreconditionCheckForAddressBounds(address);
            _arrayDescriptors[address] = descriptor;
        }

        public ArrayDescriptor this[int i] {
            get { return Get(i); }
            set { Set(i, value); }
        }

        private void PreconditionCheckForSizeLimit() {
            if (Size >= MaxArrayDescriptors)
                throw new RegisterLimitException("Maximum number of Array Descriptors have been registered");
        }

        private void PreconditionCheckForAddressBounds(int address) {
            if (Size < 1) throw new EmptyRegisterException(address);
            AddressBoundary.ValidateAddress(address, 0, Size - 1, "Array Descriptor");
        }

    }
}
