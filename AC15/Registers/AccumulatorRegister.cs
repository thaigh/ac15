﻿using System;
using AC15.Memory;
using AC15.Exceptions;

namespace AC15.Registers
{

    public class AccumulatorValue {
        private IMemory _value;
        public MemoryType CurrentMemoryType => _value.MemoryType;

        public void Set(IMemory mem) { _value = mem; }

        public IMemory Get() {
            switch(CurrentMemoryType)
            {
                case MemoryType.FloatingPoint: return new FloatingPointMemory(GetFloatingPoint());
                case MemoryType.Boolean: return new BooleanMemory(GetBoolean());
                case MemoryType.MemoryAddress: return new MemoryAddress(GetAddress());
                default: throw new ArgumentException("Unable to convert Accumulator Value to concrete type");
            }
        }

        public double GetFloatingPoint() {
            CheckMemoryType(MemoryType.FloatingPoint);
            return ((FloatingPointMemory)_value).Value;
        }

        public bool GetBoolean() {
            CheckMemoryType(MemoryType.Boolean);
            return ((BooleanMemory)_value).Value;
        }

        public int GetAddress() {
            CheckMemoryType(MemoryType.MemoryAddress);
            return ((MemoryAddress)_value).AddressIndex;
        }

        public void SetFloatingPoint(double f) { _value = new FloatingPointMemory(f); }
        public void SetBoolean(bool b) { _value = new BooleanMemory(b); }
        public void SetAddress(int address) { _value = new MemoryAddress(address); }



        private void CheckMemoryType(MemoryType type) {
            if (CurrentMemoryType != type)
                throw new InvalidMemoryCastException(type, CurrentMemoryType);
        }

        // Handles assignment implicit casting
        public static implicit operator AccumulatorValue(double right) {
            AccumulatorValue acc = new AccumulatorValue();
            acc.SetFloatingPoint(right);
            return acc;
        }

        public static implicit operator AccumulatorValue(bool right) {
            AccumulatorValue acc = new AccumulatorValue();
            acc.SetBoolean(right);
            return acc;
        }

        public override string ToString()
        {
            switch(CurrentMemoryType) {
                case MemoryType.FloatingPoint: return ((FloatingPointMemory)_value).Value.ToString();
                case MemoryType.Boolean: return ((BooleanMemory)_value).Value.ToString();
            }

            return "[Undefined]";
        }

    }

    public class AccumulatorRegister
    {
        //public readonly AccumulatorValue Value;
        public AccumulatorValue Value { get; set; }

        public override string ToString()
        {
            return string.Format("[AccumulatorRegister: CurrentMemoryType={0}, Value={1}]", Value.CurrentMemoryType, Value.ToString());
        }
    }
}
