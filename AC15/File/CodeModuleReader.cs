﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using AC15.Registers;
using AC15.Memory;
using AC15.Exceptions;
using AC15.Extensions;

namespace AC15.File
{
    public class CodeModuleReader
    {
        private StreamReader _reader;
        private string _currentLine = "";
        private RegisterGroup _group = new RegisterGroup();

        private bool ReadLineFromFile() {
            _currentLine = _reader.ReadLine();
            return _currentLine != null && !_currentLine.Contains("END");
        }

        public RegisterGroup LoadCodeModuleFromFile(string filePath) {
            using (FileStream file = new FileStream(filePath, FileMode.Open)) {
                return LoadCodeModuleFromStream(file);
            }
        }

        public RegisterGroup LoadCodeModuleFromText(string codeModule) {
            using (Stream s = codeModule.ToStream()) {
                return LoadCodeModuleFromStream(s);
            }
        }

        private RegisterGroup LoadCodeModuleFromStream(Stream s) {
            _currentLine = "";
            _group = new RegisterGroup();

            using (_reader = new StreamReader(s))
            {
                while (ReadLineFromFile())
                {
                    if (_currentLine == "") continue;
                    DecodeLineFromCodeModule(_currentLine);
                }
            }

            return _group;
        }

        private void DecodeLineFromCodeModule(string line) {
            if (line.Contains("WORD")) BuildFloatWord(line);
            if (line.Contains("BLOCK")) BuildArrayBlock(line);
            if (line.Contains("STRING")) BuildString(line);
            if (line.Contains("CODE")) BuildCodeBlock(line);
        }

        private static readonly Regex FloatRegex = new Regex(@"-?\d+(\.\d+)?");
        private void BuildFloatWord(string line) {
            // Line is of the form:
            // "WORD 5.0"
            float f = ExtractFloatFromLine(line);
            _group.Ram.AddFloatingPoint(f);
        }

        private float ExtractFloatFromLine(string line) {
            Match m = FloatRegex.Match(line);
            float f = float.Parse(m.Value);
            return f;
        }

        private static readonly Regex IntegerRegex = new Regex(@"-?\d+");
        private void BuildArrayBlock(string line) {
            // Line is of the form:
            // "BLOCK 3"
            int blockSize = ExtractIntFromLine(line);

            // IMPROVE: Use a Float Regex and check for integer
            if (blockSize <= 0)
                throw new CodeModuleException("Array BLOCK size must be a positive integer");

            ArrayDescriptor blockDescriptor = new ArrayDescriptor(blockSize, _group.Ram.NextAddress);
            _group.ArrayDescriptors.Add(blockDescriptor);
            _group.Ram.AddArrayBlock(blockDescriptor);
        }

        private int ExtractIntFromLine(string line) {
            Match m = IntegerRegex.Match(line);
            int i = int.Parse(m.Value);
            return i;
        }

        private void BuildString(string line) {
            // Line is of the form:
            //"STRING 2"
            //   65 66 67 68 69 70 71 72
            //   73 74 75 76 77 78 79 80

            // We need to read n lines to build multiple string memory words
            // where n is specified on the first line
            int stringWordLength = ExtractIntFromLine(line);

            if (stringWordLength <= 0)
                throw new CodeModuleException("STRING word length must be a positive integer");

            for (int i = 0; i < stringWordLength; i++) {
                BuildStringByteData();
            }

        }

        private void BuildStringByteData() {
            // Read next line from file
            if (!ReadLineFromFile())
                throw new CodeModuleException("Missing STRING Byte Data");

            // Extract char array from line. There should be 8 chars on a line
            int[] bytes = ExtractByteDataFromLine(_currentLine, StringMemory.WordLength);

            // Convert the int[] to a char[]
            char[] charData = new char[bytes.Length];
            for (int i = 0; i < bytes.Length; i++) { charData[i] = (char)bytes[i]; }

            // Add the memory word
            _group.Ram.AddString(charData);
        }

        private int[] ExtractByteDataFromLine(string line, int expectedCount) {
            MatchCollection matches = MatchByteDatafromLine(line, expectedCount);
            int[] byteData = ExtractByteDataFromRegexMatchCollection(matches);
            return byteData;
        }

        private MatchCollection MatchByteDatafromLine(string line, int expectedCount) {
            MatchCollection matches = IntegerRegex.Matches(line);
            if (matches.Count != expectedCount)
                throw new CodeModuleException(
                    string.Format("Expected a byte array of length {0}, but got length of {1}", expectedCount, matches.Count)
                );

            return matches;
        }

        private int[] ExtractByteDataFromRegexMatchCollection(MatchCollection matches) {
            int[] byteData = new int[matches.Count];
            int byteIndex = 0;

            foreach (Match m in matches) {
                int i = int.Parse(m.Value);
                byteData[byteIndex++] = i;
            }

            return byteData;
        }

        private void BuildCodeBlock(string line) {
            // Line is of the form:
            // CODE 2
            //  66   0   0  26  68   0   0
            //   0  23  69   0   0  72   0

            int codeBlockLength = ExtractIntFromLine(line);

            if (codeBlockLength <= 0)
                throw new CodeModuleException("CODE block length must be a positive integer");

            _group.SubprogramDescriptors.Add(new SubprogramDescriptor(_group.Instructions.NextAddress, 0));

            for (int i = 0; i < codeBlockLength; i++) {
                BuildCodeBlockData();
            }
        }

        private void BuildCodeBlockData() {
            // Read line from file
            if (!ReadLineFromFile())
                throw new CodeModuleException("Missing CODE block byte data");

            // Extract byte data from line
            int[] byteData = ExtractByteDataFromLine(_currentLine, InstructionMemory.WordLength);
            _group.Instructions.Add(byteData);
        }


    }
}
